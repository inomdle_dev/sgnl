/**
 * File name : favorite_manager.c
 *
 * This file contains the source code for managing favorite call data
 */

#include "peripheral_define.h"

#define FAVORITE_DATA_INDEX		5
#define FAVORITE_DATA_SIZE	20

#define FAVORITE_COMP		0
#define FAVORITE_INDEX		1
#define FAVORITE_FLAG		2
#define FAVORITE_TARGET		3
#define FAVORITE_NUMBER		4
#define FAVORITE_NAME		12

#ifdef ENABLE_FAVORITE_MANAGER

enum {
	FAVORITE_PARSE_DELETE	= 0,
	FAVORITE_PARSE_NUMBER	= 1,
	FAVORITE_PARSE_NAME		= 2,
	FAVORITE_PARSE_STATUS	= 3,
};

void favorite_data_delete( uint8_t u8_index )
{
	static uint32_t flash_favorite_data[FAVORITE_DATA_INDEX][FAVORITE_DATA_SIZE];

	// flash read
	for( int i = 0; i < FAVORITE_DATA_INDEX; i++ )
	{
		// new data
		if( i == u8_index )
		{
			for( int j = 0; j < FAVORITE_DATA_SIZE; j++ )
			{
				flash_favorite_data[i][j] = 0xFFFFFFFF;
			}
		}
		else
		{
			user_flash_read( USER_FLASH_FAVORITE, flash_favorite_data[i], i, (uint32_t)sizeof(flash_favorite_data[i]));
			
			// 0 : equal
			if( strncmp((char *)flash_favorite_data[i], "favdata", 7) )
			{
				for( int j = 0; j < FAVORITE_DATA_SIZE; j++ )
				{
					flash_favorite_data[i][j] = 0xFFFFFFFF;
				}
			}
		}
	}
	
	user_flash_erase( USER_FLASH_FAVORITE );
	
	user_flash_write(USER_FLASH_FAVORITE, (uint32_t *)flash_favorite_data, 0, (uint32_t)sizeof(flash_favorite_data));
	
	favorite_log_delete(u8_index);
}

void favorite_data_write( uint8_t u8_index )
{
	static uint32_t flash_favorite_data[FAVORITE_DATA_INDEX][FAVORITE_DATA_SIZE];

	// flash read
	for( int i = 0; i < FAVORITE_DATA_INDEX; i++ )
	{
		// new data
		if( i == u8_index )
		{
			strncpy((char *)(&flash_favorite_data[i][FAVORITE_COMP]), "favdata", 7);
			flash_favorite_data[i][FAVORITE_INDEX] |= ( uint32_t )( ( g_str_favo_mgr.ui8_index & 0x00FF ) << 24 );
			strncpy((char *)(&flash_favorite_data[i][FAVORITE_NUMBER]), (char *)(g_str_favo_mgr.ui8_number), g_str_favo_mgr.ui8_num_len);
			//strncpy((char *)(&flash_favorite_data[i][FAVORITE_NAME]), (char *)(g_str_favo_mgr.ui8_name), 16);
			uint8_t *tmp_data = (uint8_t *)(&flash_favorite_data[i][FAVORITE_NAME]);
			for( int j = 0; i < g_str_favo_mgr.ui8_name_len; i++ )
			{
				tmp_data[j] = g_str_favo_mgr.ui8_name[j];
			}

			flash_favorite_data[i][FAVORITE_FLAG] = ( uint32_t )( ( g_str_favo_mgr.ui8_log_type & 0x00FF ) << 24 );
			flash_favorite_data[i][FAVORITE_FLAG] |= ( uint32_t )( ( g_str_favo_mgr.ui8_log_enable & 0x00FF ) << 16 );
			flash_favorite_data[i][FAVORITE_FLAG] |= ( uint32_t )( ( g_str_favo_mgr.ui8_num_len & 0x00FF ) << 8 );
			flash_favorite_data[i][FAVORITE_FLAG] |= ( uint32_t )( g_str_favo_mgr.ui8_name_len & 0x00FF );
			flash_favorite_data[i][FAVORITE_TARGET] = ( uint32_t )( g_str_favo_mgr.ui16_target_cnt << 16 );
			flash_favorite_data[i][FAVORITE_TARGET] |= ( uint32_t )( g_str_favo_mgr.ui16_target_time & 0x00FFFF );
		}
		else
		{
			user_flash_read( USER_FLASH_FAVORITE, flash_favorite_data[i], i, (uint32_t)sizeof(flash_favorite_data[i]));
			
			// 0 : equal
			if( strncmp((char *)flash_favorite_data[i], "favdata", 7) )
			{
				for( int j = 0; j < FAVORITE_DATA_SIZE; j++ )
				{
					flash_favorite_data[i][j] = 0xFFFFFFFF;
				}
			}
		}
	}
	
	user_flash_erase( USER_FLASH_FAVORITE );
	
	user_flash_write(USER_FLASH_FAVORITE, (uint32_t *)flash_favorite_data, 0, (uint32_t)sizeof(flash_favorite_data));
	
	favorite_log_write(u8_index);
}

void favorite_data_parser_group( uint8_t *u8_data, uint8_t u8_data_len )
{
	uint8_t u8_data_type = u8_data[0];
	
	switch( u8_data_type )
	{
	case REP_FAVO_SEND_FIRST_NUM:
		strncpy((char *)(g_str_favo_mgr.ui8_number), (char *)(&u8_data[1]), u8_data_len);
		g_str_favo_mgr.ui8_num_len = u8_data_len;
		break;
	case REP_FAVO_SEND_ADD_NUM:
		strncpy((char *)(&g_str_favo_mgr.ui8_number[g_str_favo_mgr.ui8_num_len]), (char *)(&u8_data[1]), u8_data_len);
		g_str_favo_mgr.ui8_num_len += u8_data_len;
		break;

	case REP_FAVO_SEND_FIRST_NAME:
	{
		//strncpy((char *)(g_str_favo_mgr.ui8_name), (char *)(&u8_data[1]), u8_data_len);
		uint8_t *tmp_data = &u8_data[1];

		for( int j = 0; j < u8_data_len; j++ )
		{
			g_str_favo_mgr.ui8_name[j] = tmp_data[j];
		}
		
		g_str_favo_mgr.ui8_name_len = u8_data_len;
		break;
	}
	case REP_FAVO_SEND_ADD_NAME:
	{
		//strncpy((char *)(&g_str_favo_mgr.ui8_name[g_str_favo_mgr.ui8_name_len]), (char *)(&u8_data[1]), u8_data_len);
		uint8_t *tmp_data = &u8_data[1];

		for( int j = 0; j < u8_data_len; j++ )
		{
			g_str_favo_mgr.ui8_name[j+g_str_favo_mgr.ui8_name_len] = tmp_data[j];
		}

		g_str_favo_mgr.ui8_name_len += u8_data_len;
		break;
	}
	case REP_FAVO_SEND_TARGET_DATA :
		if( u8_data[1] > 9 )
		{
			g_str_favo_mgr.ui8_index = u8_data[1] - '0';
		}
		else
		{
			g_str_favo_mgr.ui8_index = u8_data[1];
		}
		g_str_favo_mgr.ui8_log_enable = u8_data[2] - '0';
		g_str_favo_mgr.ui8_log_type = u8_data[3] - '0';
		
		if( g_str_favo_mgr.ui8_log_type == 1 )
		{
			g_str_favo_mgr.ui16_target_cnt = u8_data[4];
		}
		else
		{
			g_str_favo_mgr.ui16_target_time = u8_data[4] * 60; // second = minute * 60
		}
		favorite_data_write( g_str_favo_mgr.ui8_index );

		g_str_bit.b1_favorite_save_ready = 0;

		break;
	default : break;
	}

	uint8_t byte[5] = "";
	uint8_t nus_index = 0;
	byte[++nus_index] = REPORT_FAVORITE_DATA;
	byte[++nus_index] = REP_FAVO_DATA_SEND;
	byte[++nus_index] = u8_data_type;
	byte[++nus_index] = 0x5A;
	
	
	byte[0] = ++nus_index;
	
	nus_tx_push(byte, byte[0]);
}

void favorite_read_number( uint8_t u8_index, bool up_button )
{
	static uint32_t flash_favorite_read[FAVORITE_DATA_SIZE];
	
	memset(&g_str_favo_mgr, 0, sizeof(favo_mgr_struct_t));

	uint8_t u8_find_num = u8_index;
	
	for( int cnt = 0; cnt < 5; cnt ++ )
	{
		if( up_button )
		{
			u8_find_num = ( u8_index + cnt ) % 5;
		}
		else
		{
			u8_find_num = ( u8_index + 5 - cnt ) % 5;
		}
		
		user_flash_read( USER_FLASH_FAVORITE, flash_favorite_read, u8_find_num, (uint32_t)sizeof(flash_favorite_read));
		
		if( !strncmp((char *)flash_favorite_read, "favdata", 7) )
		{
			g_str_favo_mgr.ui8_index = u8_find_num;
			
			u8_find_num = 5;
			break;
		}
	}
	
	if( u8_find_num != 5 )
	{
		g_str_favo_mgr.ui8_num_len = 0;
		return;
	}

	g_str_favo_mgr.ui8_name_len = ( uint8_t )( flash_favorite_read[FAVORITE_FLAG] & 0x0000FF );
	g_str_favo_mgr.ui8_num_len = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0x0000FF00 ) >> 8 );
	g_str_favo_mgr.ui8_log_enable = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0x00FF0000 ) >> 16 );
	g_str_favo_mgr.ui8_log_type = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0xFF000000 ) >> 24 );
	g_str_favo_mgr.ui16_target_cnt = ( uint16_t )( ( flash_favorite_read[FAVORITE_TARGET] >> 16 ) & 0x00FFFF );
	g_str_favo_mgr.ui16_target_time = ( uint16_t )( flash_favorite_read[FAVORITE_TARGET] & 0x00FFFF );

	strncpy((char *)(g_str_favo_mgr.ui8_number), (char *)(&flash_favorite_read[FAVORITE_NUMBER]), g_str_favo_mgr.ui8_num_len);
	//strncpy((char *)(g_str_favo_mgr.ui8_name), (char *)(&flash_favorite_read[FAVORITE_NAME]), 16);
	uint8_t *tmp_data = (uint8_t *)(&flash_favorite_read[FAVORITE_NAME]);
	
	for( int j = 0; j < g_str_favo_mgr.ui8_name_len; j++ )
	{
		g_str_favo_mgr.ui8_name[j] = tmp_data[j];
	}
	
	favorite_log_read( g_str_favo_mgr.ui8_index );
}

uint8_t favorite_name_find( uint8_t *u8_data, uint8_t u8_size )
{
	static uint32_t flash_favorite_read[FAVORITE_DATA_SIZE];

	// flash read
	for( int i = 0; i < FAVORITE_DATA_INDEX; i++ )
	{
		memset(&g_str_favo_mgr, 0, sizeof(favo_mgr_struct_t));
		
		user_flash_read( USER_FLASH_FAVORITE, flash_favorite_read, i, (uint32_t)sizeof(flash_favorite_read));

		//strncpy((char *)(g_str_favo_mgr.ui8_name), (char *)(&flash_favorite_read[FAVORITE_NAME]), g_str_favo_mgr.ui8_name_len);
		uint8_t *tmp_data = (uint8_t *)(&flash_favorite_read[FAVORITE_NAME]);
		int j = 0;
		for( j = 0; j < g_str_favo_mgr.ui8_name_len; j++ )
		{
			if( tmp_data[j] != u8_data[j] )
			{
				j = 0;
				break;
			}
		}		

		if( j == g_str_favo_mgr.ui8_name_len )
		{
			g_str_favo_mgr.ui8_index = i;
			
			favorite_log_read(i);
			
			return 1;
		}
	}
	return 0;
}


#endif //ENABLE_FAVORITE_MANAGER
