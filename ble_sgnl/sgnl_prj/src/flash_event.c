/**
 * File name : flash_event.c
 *
 * This file contains the source code for a managing flash R/W/erase
 */

#include "peripheral_define.h"

#ifdef ENABLE_FLASH
/**@brief Function for flash erase.
 *
 * @param[in]   list   Select Flash page.
 */
#define PAGE_SIZE_WORDS	1024
	 
static void fs_user_evt_handler(fs_evt_t const * const evt, fs_ret_t result)
{
    if (result != FS_SUCCESS)
    {
        // An error occurred.
    }
}
	 
FS_REGISTER_CFG(fs_config_t fs_user_config) =
{
    .callback  = fs_user_evt_handler, // Function for event callbacks.
    .num_pages = USER_FLASH_LIST,      // Number of physical flash pages required.
    .priority  = 0xFE            // Priority for flash usage.
};


// Retrieve the address of a page.
static uint32_t const * address_of_page(uint16_t page_num)
{
    return fs_user_config.p_start_addr + (page_num * PAGE_SIZE_WORDS);
}

void user_flash_init( void )
{
	fs_ret_t ret = fs_init();
	if (ret != FS_SUCCESS)
	{
		// An error occurred.
	}
}


void user_str_lower( uint8_t *ui8_data, uint8_t ui8_len )
{
	for( int i = 0; i < ui8_len; i++ )
	{
		if( ( ui8_data[i] >= 'A' ) && ( ui8_data[i] <= 'Z' ) )
		{
			ui8_data[i] += 0x20;
		}
	}
}
	 
void user_flash_erase( user_flash_list_t list )
{
	fs_ret_t ret;
	// Erase one page (page list).

	ret = fs_erase(&fs_user_config, address_of_page(list), 1, NULL);
	while (ret != FS_SUCCESS)
	{
		// An error occurred.
		ret = fs_erase(&fs_user_config, address_of_page(list), 1, NULL);
	}
}

void user_flash_all_erase(void)
{
	fs_ret_t ret;

	ret = fs_erase(&fs_user_config, address_of_page(0), USER_FLASH_LIST, NULL);
	while (ret != FS_SUCCESS)
	{
		// An error occurred.
		ret = fs_erase(&fs_user_config, address_of_page(0), USER_FLASH_LIST, NULL);
	}
}
/**@brief Function for flash write.
 *
 * @param[in]   list		Select Flash page.
 * @param[in]   ui32_data	Write data.
 * @param[in]   ui32_cnt	Write index.
 * @param[in]   ui32_size	Write size.
 */
void user_flash_write( user_flash_list_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size )
{
	uint32_t ui32_index = ui32_size / sizeof(uint32_t);
	
	uint32_t * start_addr = (uint32_t *)( address_of_page(list) + (ui32_index * ui32_cnt) );
	
	fs_ret_t ret = fs_store(&fs_user_config, start_addr, ui32_data, ui32_index, NULL);
	while (ret != FS_SUCCESS)
	{
		// An error occurred.
		ret = fs_store(&fs_user_config, start_addr, ui32_data, ui32_index, NULL);
	}
}

/**@brief Function for flash read.
 *
 * @param[in]   list		Select Flash page.
 * @param[in]   ui32_data	read data.
 * @param[in]   ui32_cnt	read index.
 * @param[in]   ui32_size	read size.
 */
void user_flash_read( user_flash_list_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size )
{
	uint32_t ui32_index = ui32_size / sizeof(uint32_t);
	uint32_t* start_address = (uint32_t *)address_of_page(list) + (ui32_index * ui32_cnt);
	
	for( int i = 0; i < ui32_index; i++ )
	{
		ui32_data[i] = *start_address;
		start_address += 1;
	}

}
#endif
