/**
 * File name : pwm_event.c
 *
 * This file contains the source code for managing PWM
 */

#include "peripheral_define.h"

#ifdef ENABLE_PWM

APP_PWM_INSTANCE(PWM1,1);                   // Create the instance "PWM1" using TIMER1.
APP_PWM_INSTANCE(PWM2,2);                   // Create the instance "PWM2" using TIMER2.
APP_PWM_INSTANCE(PWM3,3);                   // Create the instance "PWM2" using TIMER3.

static volatile bool ready_flag, ready_flag2, ready_flag3;            // A flag indicating PWM status.

void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    ready_flag = true;
}

void pwm_ready_callback2(uint32_t pwm_id)    // PWM callback function
{
    ready_flag2 = true;
}

void pwm_ready_callback3(uint32_t pwm_id)    // PWM callback function
{
    ready_flag3 = true;
}

void pwm_enable(void)
{
	if( !g_str_bit.b1_pwm_enable )
	{
		g_str_bit.b1_pwm_enable = 1;
		app_pwm_enable(&PWM1);
		app_pwm_enable(&PWM2);
		app_pwm_enable(&PWM3);
	}
}

void pwm_disable(void)
{
	pwm_off();
	pattern_timer_stop();
}

void pwm_led_1(uint32_t ui32_duty)
{
	if( ui32_duty == 0 )
	{
		pwm_off();
	}
	
	if( app_pwm_channel_duty_get(&PWM1, 0) != ui32_duty )
	{
		pwm_enable();
		while (app_pwm_channel_duty_set(&PWM1, 0, ui32_duty) == NRF_ERROR_BUSY);
	}
}

void pwm_led_2(uint32_t ui32_duty)
{
	if( ui32_duty == 0 )
	{
		pwm_off();
	}
	
	if( app_pwm_channel_duty_get(&PWM1, 1) != ui32_duty )
	{
		pwm_enable();
		while (app_pwm_channel_duty_set(&PWM1, 1, ui32_duty) == NRF_ERROR_BUSY);
	}
}

void pwm_led_3(uint32_t ui32_duty)
{
	if( ui32_duty == 0 )
	{
		pwm_off();
	}
	
	if( app_pwm_channel_duty_get(&PWM2, 0) != ui32_duty )
	{
		pwm_enable();
		while (app_pwm_channel_duty_set(&PWM2, 0, ui32_duty) == NRF_ERROR_BUSY);
	}
}

void pwm_led_4(uint32_t ui32_duty)
{
	if( ui32_duty == 0 )
	{
		pwm_off();
	}
	
	if( app_pwm_channel_duty_get(&PWM2, 1) != ui32_duty )
	{
		pwm_enable();
		while (app_pwm_channel_duty_set(&PWM2, 1, ui32_duty) == NRF_ERROR_BUSY);
	}
}

void pwm_led_5(uint32_t ui32_duty)
{
	if( ui32_duty == 0 )
	{
		pwm_off();
	}
	
	if( app_pwm_channel_duty_get(&PWM3, 0) != ui32_duty )
	{
		pwm_enable();
		while (app_pwm_channel_duty_set(&PWM3, 0, ui32_duty) == NRF_ERROR_BUSY);
	}
}

void pwm_off(void)
{
	g_str_bit.b1_pwm_enable = 0;
		
	app_pwm_disable(&PWM1);
	app_pwm_disable(&PWM2);
	app_pwm_disable(&PWM3);
}

void pwm_init(void)
{
	uint32_t err_code;
	/* 2-channel PWM, 200Hz, output on DK LED pins. */
    app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, BSP_LED_0, BSP_LED_1);
	app_pwm_config_t pwm2_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, BSP_LED_2, BSP_LED_3);
	app_pwm_config_t pwm3_cfg = APP_PWM_DEFAULT_CONFIG_1CH(5000L, BSP_LED_4);

    /* Switch the polarity of the second channel. */
	//pwm1_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;

    /* Initialize and enable PWM. */
    err_code = app_pwm_init(&PWM1,&pwm1_cfg,pwm_ready_callback);
    APP_ERROR_CHECK(err_code);
	
	err_code = app_pwm_init(&PWM2,&pwm2_cfg,pwm_ready_callback2);
    APP_ERROR_CHECK(err_code);
	
	err_code = app_pwm_init(&PWM3,&pwm3_cfg,pwm_ready_callback3);
    APP_ERROR_CHECK(err_code);
}

#endif //ENABLE_PWM
