/**
 * File name : pattern_control.c
 *
 * This file contains the source code for LED/haptic control
 */

#include "peripheral_define.h"

#ifdef ENABLE_PWM

void ( *pwm_pulse_led[5] )( uint32_t ) = { pwm_led_5, pwm_led_4,
								pwm_led_3, pwm_led_2, pwm_led_1 };

/**@brief Function for pwm data increase.
 *
 * @param[in]   index		led index.
 * @param[in]   add_val		add value.
 */
static void add_pwm( uint16_t index, uint32_t add_val )
{
	pwm_pulse_led[index](g_str_led[index].ui32_pwm);

	g_str_led[index].ui32_pwm += add_val;
	if( g_str_led[index].ui32_pwm > 100 )
	{
		g_str_led[index].ui32_pwm = 100;
	}
}

/**@brief Function for pwm data increase all LED.
 *
 * @param[in]   add_val		add value.
 */
static void add_pwm_all(uint32_t add_val)
{
	add_pwm(0, add_val);
	add_pwm(1, add_val);
	add_pwm(2, add_val);
	add_pwm(3, add_val);
	add_pwm(4, add_val);
}

/**@brief Function for pwm data decrease.
 *
 * @param[in]   index		led index.
 * @param[in]   sub_val		sub value.
 */
static void sub_pwm( uint16_t index, uint32_t sub_val )
{
	pwm_pulse_led[index](g_str_led[index].ui32_pwm);

	if( g_str_led[index].ui32_pwm > sub_val )
	{
		g_str_led[index].ui32_pwm -= sub_val;
	}
	else
	{
		g_str_led[index].ui32_pwm = 0;
	}
}

/**@brief Function for pwm data decrease all LED.
 *
 * @param[in]   sub_val		sub value.
 */
static void sub_pwm_all(uint32_t sub_val)
{
	sub_pwm(0, sub_val);
	sub_pwm(1, sub_val);
	sub_pwm(2, sub_val);
	sub_pwm(3, sub_val);
	sub_pwm(4, sub_val);
}

/**@brief Function for pwm data set all LED.
 *
 * @param[in]   val		set value.
 */

static void set_pwm_all( uint32_t val )
{
	pwm_pulse_led[0](val);
	pwm_pulse_led[1](val);
	pwm_pulse_led[2](val);
	pwm_pulse_led[3](val);
	pwm_pulse_led[4](val);
}

/**@brief Function for pwm data set.
 *
 * @param[in]   index	led index.
 * @param[in]   val		set value.
 */
static void set_pwm( uint16_t index, uint32_t value )
{
	pwm_pulse_led[index](value);
}

void pattern_power_off( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_POWER_OFF_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 10 )
	{
		add_pwm_all(3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 20 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 30 )
	{
		sub_pwm(4, 10);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 40 )
	{
		set_pwm(4, 0);
		sub_pwm(3, 10);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 50 )
	{
		set_pwm(3, 0);
		sub_pwm(2, 10);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 60 )
	{
		set_pwm(2, 0);
		sub_pwm(1, 10);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 70 )
	{
		set_pwm(1, 0);
		sub_pwm(0, 10);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 80 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 90 )
	{
		add_pwm_all(5);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 95 )
	{
		sub_pwm_all(5);
	}
	else
	{
		uint32_t ret;
		
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		
		ret = sd_power_system_off();
    	APP_ERROR_CHECK(ret);
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 82 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 94 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_power_on( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_POWER_ON_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 20 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 32 )
	{
		set_pwm(1, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 44 )
	{
		set_pwm(2, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 56 )
	{
		set_pwm(3, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 70 )
	{
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 86 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 94 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 100 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 105 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 80 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 92 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_device_reset( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DEVICE_RESET_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 5 )
	{
		add_pwm_all(6);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 10 )
	{
		if( g_str_timer.ui16_pattern_cnt % 2 )
		{
			sub_pwm_all(1);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 60 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		
		//device reset
		 // Go to system-off mode (this function will not return; wakeup will cause a reset).
		//ret_code_t ret = sd_power_system_off();
		call_power_off();
		//APP_ERROR_CHECK(ret);
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DEVICE_RESET_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 6 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_blink_gauge( uint16_t jump_cnt, uint16_t index )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt > jump_cnt )
	{
		for( int i = 0; i <= index; i++ )
		{
			set_pwm(i, 100);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > jump_cnt - 28 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > jump_cnt - 36 )
	{
		for( int i = 0; i <= index; i++ )
		{
			set_pwm(i, 100);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > jump_cnt - 44 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > jump_cnt - 52 )
	{
		for( int i = 0; i <= index; i++ )
		{
			set_pwm(i, 100);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > jump_cnt - 60 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
}

void pattern_batt_gauge( void )
{
	uint16_t jump_cnt;
	switch ( g_str_batt.ui16_level )
	{
	case 0 :
		jump_cnt = PATTERN_BATT_GAUGE_CNT - 16;
		break;
	case 1 :
		jump_cnt = PATTERN_BATT_GAUGE_CNT - 24;
		break;
	case 2 :
		jump_cnt = PATTERN_BATT_GAUGE_CNT - 32;
		break;
	case 3 :
		jump_cnt = PATTERN_BATT_GAUGE_CNT - 40;
		break;
	case 4 :
		jump_cnt = PATTERN_BATT_GAUGE_CNT - 48;
		break;
	}
	
	// led control	
	if( g_str_timer.ui16_pattern_cnt <= jump_cnt )
	{
		pattern_blink_gauge( jump_cnt, g_str_batt.ui16_level );
		return;
	}
	
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_GAUGE_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 16 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 24 )
	{
		set_pwm(1, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 32 )
	{
		set_pwm(2, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 40 )
	{
		set_pwm(3, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 48 )
	{
		set_pwm(4, 100);
	}
}

void pattern_batt_low( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_LOW_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 15 )
	{
		set_pwm(0,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 30 )
	{
		set_pwm(0,0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 45 )
	{
		set_pwm(0,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 60 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_LOW_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 6 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_batt_charging( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_CHARGING_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 5 )
	{
		for( int i = 0; i < g_str_batt.ui16_level; i++ )
		{
			set_pwm(i, 100);
		}
		if( !g_str_batt.ui16_level )
		{
			set_pwm(g_str_batt.ui16_level, 100);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 30 )
	{
		for( int i = 0; i < g_str_batt.ui16_level; i++ )
		{
			set_pwm(i, 100);
		}
		set_pwm(g_str_batt.ui16_level, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 40 )
	{
		for( int i = 0; i < g_str_batt.ui16_level; i++ )
		{
			set_pwm(i, 100);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 70 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		
		if( g_str_bit.b1_chg_status )
		{
			call_batt_charging();
		}
	}
}

void pattern_batt_full( void )
{
	set_pwm_all(100);
}

void pattern_unpaired( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_UNPAIRED_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_UNPAIRED_CNT - 90 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_UNPAIRED_CNT - 150 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		
		if( !g_str_app_status.ui8_ble_bonded )
		{
			call_unpaired();
		}
	}
}

void pattern_unpair( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_UNPAIR_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_UNPAIR_CNT - 23 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_UNPAIR_CNT - 36 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
}

void pattern_wait_pairing( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_WAIT_PAIRING_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 10 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 18 )
	{
		set_pwm(1, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 28 )
	{
		set_pwm(2, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 37 )
	{
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 40 )
	{
		set_pwm(3, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 53 )
	{
		set_pwm(2, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 73 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(3, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 81 )
	{
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 90 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		
		if( g_str_bit.b1_wait_pairing )
		{
			call_wait_pairing();
		}
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 7 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 13 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
		
}

void pattern_pairing_complete( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_PAIRING_COMPLETE_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 12 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 34 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 9 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 15 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_disconn_timeout( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DISCONN_TIMEOUT_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 10 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 15 )
	{
		set_pwm(0, 0);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 25 )
	{
		set_pwm(0, 100);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 30 )
	{
		set_pwm(0, 0);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 40 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 9 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 11 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 15 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 20 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 24 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 26 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 30 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 35 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

#ifdef ENABLE_DND
void pattern_dnd_on( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DND_ON_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 10 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 18 )
	{
		set_pwm(1, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 28 )
	{
		set_pwm(2, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 40 )
	{
		set_pwm(3, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 53 )
	{
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 73 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;

		if( g_str_bit.b1_haptic_status )
		{
			g_str_bit.b1_haptic_status = 0;
		}
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 7 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 13 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_dnd_off( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DND_OFF_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 10 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 30 )
	{
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 35 )
	{
		set_pwm(3, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 45 )
	{
		set_pwm(2, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 55 )
	{
		set_pwm(1, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 65 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 7 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 13 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}
#endif //ENABLE_DND

#ifdef ENABLE_FAVORITE_MANAGER
void pattern_favorite_select( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_SELECT_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_SELECT_CNT - 6 )
	{
		set_pwm_all(0);
		set_pwm(g_str_favo_mgr.ui8_index, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_SELECT_CNT - 30 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_SELECT_CNT - 36 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		
		if( g_str_call_bit.b1_favorite_enable )
		{
			call_favorite_select();
		}
	}
}

void pattern_favorite_call( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_CALL_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 14 )
	{
		set_pwm(g_str_favo_mgr.ui8_index, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 28 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		if( g_str_call_bit.b4_call_status & CALL_STATUS_FAVORITE )
		{
			if( g_str_call_bit.b4_call_status & CALL_STATUS_OUTGOING )
			{
				call_favorite_call();
			}
		}
	}
}

void pattern_incoming_favorite( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_INCOMING_CALL_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 15 )
	{
		set_pwm(g_str_favo_mgr.ui8_index, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 26 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 31 )
	{
		set_pwm(g_str_favo_mgr.ui8_index, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 38 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 43 )
	{
		set_pwm(g_str_favo_mgr.ui8_index, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 51 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 60 )
	{
		set_pwm(g_str_favo_mgr.ui8_index, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 80 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 96 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;

		if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
		{
			call_incoming_favorite();
		}
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 14 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 22 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 31 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 34 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 43 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 46 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 62 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 76 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

#endif //ENABLE_FAVORITE_MANAGER

void pattern_incoming_call( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_INCOMING_CALL_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 15 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 26 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 31 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 38 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 43 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 51 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 60 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 80 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 96 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;

		if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
		{
			call_incoming_call();
		}
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 14 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 22 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 31 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 34 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 43 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 46 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 62 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 76 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_activity_noti( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_ACTIVITY_NOTI_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 12 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 18 )
	{
		set_pwm(1, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 24 )
	{
		set_pwm(2, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 30 )
	{
		set_pwm(3, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 36 )
	{
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 55 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 60 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 65 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 70 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 75 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 80 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 88 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}

	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 17 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 31 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 56 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 60 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 66 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 80 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 86 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_long_sit_noti( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_LONG_SIT_NOTI_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 5 )
	{
		set_pwm(0,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 10 )
	{
		set_pwm(1,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 15 )
	{
		set_pwm(0,0);
		set_pwm(2,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 20 )
	{
		set_pwm(1,0);
		set_pwm(3,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 25 )
	{
		set_pwm(2,0);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 30 )
	{
		set_pwm(3,0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 35 )
	{
		set_pwm(3,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 40 )
	{
		set_pwm(4,0);
		set_pwm(2,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 45 )
	{
		set_pwm(3,0);
		set_pwm(1,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 50 )
	{
		set_pwm(2,0);
		set_pwm(0,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 55 )
	{
		set_pwm(1,0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 60 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 5 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 25 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 35 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 55 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_app_noti( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_APP_NOTI_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 10 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 18 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 26 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 35 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 8 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 14 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 24 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 30 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void sub_pattern_activity_noti( void )
{
	//haptic control
	if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 17 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 31 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 56 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 60 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 66 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 80 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 86 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void sub_pattern_long_sit_noti( void )
{
	//haptic control
	if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 5 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 25 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 35 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 55 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void sub_pattern_app_noti( void )
{
	//haptic control
	if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 8 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 14 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 24 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 30 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

#ifdef ENABLE_IFA_DEMO
void pattern_ble_conn_success( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BLE_CONN_CHK_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 20 )
	{
		set_pwm(0,100);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 40 )
	{
		set_pwm(1,100);
		set_pwm(3,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 60 )
	{
		set_pwm(2,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 80 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 100 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 15 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 30 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 60 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}
void pattern_ble_disconn( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BLE_CONN_CHK_CNT )
	{
		set_pwm_all(95);
		
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 20 )
	{
		set_pwm_all(0);
		set_pwm(0,95);
		set_pwm(1,95);
		set_pwm(3,95);
		set_pwm(4,95);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 40 )
	{
		set_pwm_all(0);
		set_pwm(0,95);
		set_pwm(4,95);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 60 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 80 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 15 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 30 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 60 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}
#endif

#endif //ENABLE_PWM
