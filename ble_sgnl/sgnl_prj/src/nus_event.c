/**
 * File name : nus_event.c
 *
 * This file contains the source code for managing ble serial data
 */

#include "peripheral_define.h"

#ifdef ENABLE_NUS_QUEUE
#include "nrf_queue.h"
#endif

#define NUS_QUEUE_SIZE	30
#define NUS_DATA_MAX	20

ble_nus_t m_nus; /**< Structure to identify the Nordic UART Service. */

#ifdef ENABLE_NUS_QUEUE
typedef struct nus_queue_data_{
	uint8_t data[NUS_DATA_MAX];
	uint8_t length;
}nus_queue_data_t;

nus_queue_data_t nus_rx_q_msg;
nus_queue_data_t nus_rx_q_msg_out;
static int nus_rx_q_cnt = 0;

NRF_QUEUE_DEF(nus_queue_data_t, m_nus_rx_q, NUS_QUEUE_SIZE, NRF_QUEUE_MODE_NO_OVERFLOW);
NRF_QUEUE_INTERFACE_DEC(nus_queue_data_t, nus_rx_q);
NRF_QUEUE_INTERFACE_DEF(nus_queue_data_t, nus_rx_q, &m_nus_rx_q);

nus_queue_data_t nus_tx_q_msg;
nus_queue_data_t nus_tx_q_msg_out;
static int nus_tx_q_cnt = 0;

NRF_QUEUE_DEF(nus_queue_data_t, m_nus_tx_q, NUS_QUEUE_SIZE, NRF_QUEUE_MODE_NO_OVERFLOW);
NRF_QUEUE_INTERFACE_DEC(nus_queue_data_t, nus_tx_q);
NRF_QUEUE_INTERFACE_DEF(nus_queue_data_t, nus_tx_q, &m_nus_tx_q);


void nus_rx_queue( void )
{
	if( nus_rx_q_cnt )
	{
		if( g_str_bit.b1_ble_send_wait == 0 )
		{
			memset(&nus_rx_q_msg_out, 0, sizeof(nus_queue_data_t) );
			nus_rx_q_pop(&nus_rx_q_msg_out);

			ble_protocol_group(nus_rx_q_msg_out.data, nus_rx_q_msg_out.length);
			nus_rx_q_cnt--;
		}
	}
}

void nus_tx_queue( void )
{
	if( nus_tx_q_cnt )
	{
		memset(&nus_tx_q_msg_out, 0, sizeof(nus_queue_data_t) );
		nus_tx_q_pop(&nus_tx_q_msg_out);
		ble_data_send(nus_tx_q_msg_out.data, nus_tx_q_msg_out.length);
		
		nus_tx_q_cnt--;
	}
}


void nus_tx_push( uint8_t* ui8_byte, uint16_t ui16_length )
{
	if( g_str_bit.b1_nus_enable )
	{
		for( int i = 0; i < ui16_length; i++ )
		{
			nus_tx_q_msg.data[i] = ui8_byte[i];
		}
		nus_tx_q_msg.length = ui16_length;
		nus_tx_q_push(&nus_tx_q_msg);
		nus_tx_q_cnt++;
	}
}

#endif

void ble_data_send( uint8_t* ui8_data, uint8_t length )
{
	uint32_t       err_code;

	if(length > NUS_DATA_MAX)
	{
		length = NUS_DATA_MAX;
		return;
	}

	g_str_bit.b1_ble_send_wait = 1;

	err_code = ble_nus_string_send(&m_nus, ui8_data, length);
	if (err_code != NRF_ERROR_INVALID_STATE)
	{
		APP_ERROR_CHECK(err_code);
	}
}

/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_nus    Nordic UART Service structure.
 * @param[in] p_data   Data to be send to UART module.
 * @param[in] length   Length of the data.
 */
/**@snippet [Handling the data received over BLE] */

static void nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data, uint16_t length)
{
	if(length > NUS_DATA_MAX)
	{
		length = NUS_DATA_MAX;
		return;
	}

#ifdef ENABLE_NUS_QUEUE
	if( p_data[0] > NUS_DATA_MAX )
	{
		uint8_t data[3];
		uint8_t ui8_index = 0;
		data[++ui8_index] = REPORT_ERR_MSG;
		data[++ui8_index] = REP_ERR_DATA_LEN;
		data[0] = ++ui8_index;				

		nus_tx_push(data,data[0]);
		return;
	}

	memset(&nus_rx_q_msg, 0, sizeof(nus_queue_data_t) );
	
	for( int i = 0; i < length; i++ )
	{
		nus_rx_q_msg.data[i] = p_data[i];
	}

	nus_rx_q_msg.length = length;
	nus_rx_q_push(&nus_rx_q_msg);
	nus_rx_q_cnt++;
	
#else
	ble_protocol_group(p_data, length);
#endif

}
/**@snippet [Handling the data received over BLE] */

void nus_service_init( void )
{
	uint32_t       err_code;
    ble_nus_init_t nus_init;

    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;

    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
}
