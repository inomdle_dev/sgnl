/**
 * File name : uart_event.c
 *
 * This file contains the source code for managing UART
 */

#include "peripheral_define.h"

#ifdef ENABLE_UART

#ifdef ENABLE_UART_QUEUE

#include "nrf_queue.h"

#define UART_QUEUE_SIZE	10

typedef struct uart_queue_data_{
	uint8_t data[20];
	uint8_t length;
}uart_queue_data_t;

uart_queue_data_t uart_rx_q_msg;
static int uart_rx_q_cnt = 0;

NRF_QUEUE_DEF(uart_queue_data_t, m_uart_rx_q, UART_QUEUE_SIZE, NRF_QUEUE_MODE_NO_OVERFLOW);
NRF_QUEUE_INTERFACE_DEC(uart_queue_data_t, uart_rx_q);
NRF_QUEUE_INTERFACE_DEF(uart_queue_data_t, uart_rx_q, &m_uart_rx_q);

uart_queue_data_t uart_tx_q_msg;
static int uart_tx_q_cnt = 0;

NRF_QUEUE_DEF(uart_queue_data_t, m_uart_tx_q, UART_QUEUE_SIZE, NRF_QUEUE_MODE_NO_OVERFLOW);
NRF_QUEUE_INTERFACE_DEC(uart_queue_data_t, uart_tx_q);
NRF_QUEUE_INTERFACE_DEF(uart_queue_data_t, uart_tx_q, &m_uart_tx_q);

void uart_rx_queue( void )
{
	if( uart_rx_q_cnt )
	{
		uart_rx_q_pop(&uart_rx_q_msg);
		mcu_protocol(uart_rx_q_msg.data, uart_rx_q_msg.length);
		uart_rx_q_cnt--;
	}
}

void uart_tx_reset( void )
{
	uart_tx_q_reset();
}

void uart_tx_queue( void )
{
	if( uart_tx_q_cnt )
	{
		uart_tx_q_pop(&uart_tx_q_msg);
		uart_send_data(uart_tx_q_msg.data, uart_tx_q_msg.length);
		
		uart_tx_q_cnt--;
	}
}

void uart_tx_push( uint8_t* ui8_byte, uint16_t ui16_length )
{
	for( int i = 0; i < ui16_length; i++ )
	{
		uart_tx_q_msg.data[i] = ui8_byte[i];
	}
	uart_tx_q_msg.length = ui16_length;
	uart_tx_q_push(&uart_tx_q_msg);
	uart_tx_q_cnt++;
}

#endif

void uart_send_data( uint8_t* ui8_byte, uint16_t ui16_length )
{
	if( !g_str_bit.b1_uart_status )
	{
		return;
	}
	
	g_str_bit.b1_uart_send_wait = 1;
	
	nrf_delay_ms(5);
	
	while(app_uart_put('$')!= NRF_SUCCESS);
	
	for( int i = 0; i < ui16_length; i++ )
	{
		while(app_uart_put(ui8_byte[i])!= NRF_SUCCESS);
    }
	while(app_uart_put(0xFA)!= NRF_SUCCESS);
	
	g_str_bit.b1_uart_send_wait = 0;
}

/*
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' '\n' (hex 0x0A) or if the string has reached the maximum data length.
 */
/**@snippet [Handling the data received over UART] */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;

            if (data_array[index - 1] == 0xFA)
            {
#ifdef ENABLE_UART_QUEUE
	
				for( int i = 0; i < index - 1; i++ )
				{
					uart_rx_q_msg.data[i] = data_array[i];
				}
				uart_rx_q_msg.length = index - 1;
				uart_rx_q_push(&uart_rx_q_msg);
				uart_rx_q_cnt++;
#else
				mcu_protocol(data_array, index - 1);
#endif

                index = 0;
            }
            break;

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}
/**@snippet [Handling the data received over UART] */

void uart_close(void)
{
	if( g_str_bit.b1_uart_status )
	{
		g_str_bit.b1_uart_status = 0;
#ifdef ENABLE_UART
		app_uart_close();
#endif //ENABLE_UART
		
		nrf_gpio_cfg( BLE_RX_PIN,
		NRF_GPIO_PIN_DIR_INPUT,
		NRF_GPIO_PIN_INPUT_CONNECT,
		NRF_GPIO_PIN_PULLUP,
		NRF_GPIO_PIN_S0S1, // NRF_GPIO_PIN_S0S1,
		NRF_GPIO_PIN_NOSENSE);
		
		nrf_gpio_cfg( BLE_TX_PIN,
		NRF_GPIO_PIN_DIR_INPUT,
		NRF_GPIO_PIN_INPUT_CONNECT,
		NRF_GPIO_PIN_NOPULL,
		NRF_GPIO_PIN_S0S1, // NRF_GPIO_PIN_S0S1,
		NRF_GPIO_PIN_NOSENSE);
	}
}

/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
void uart_init(void)
{
    uint32_t                     err_code;
    const app_uart_comm_params_t comm_params =
    {
        .rx_pin_no    = BLE_RX_PIN,
        .tx_pin_no    = BLE_TX_PIN,
        .flow_control = HWFC,
        .use_parity   = false,
        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };
	
	if( g_str_bit.b1_uart_status )
    {
       return;
    }

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);
	
	g_str_bit.b1_uart_status = 1;
	
	nrf_gpio_cfg_input(BLE_RX_PIN, NRF_GPIO_PIN_PULLUP);
}
/**@snippet [UART Initialization] */

#endif
