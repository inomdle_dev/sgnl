/**
 * File name : timer_event.c
 *
 * This file contains the source code for managing TIMER
 */

#include "peripheral_define.h"

#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */

#ifdef ENABLE_PWM
APP_TIMER_DEF(m_pattern_timer_id);                  /**< Connection parameters timer. */

APP_TIMER_DEF(m_sub_pattern_timer_id);                  /**< Connection parameters timer. */
#endif //ENABLE_PWM
APP_TIMER_DEF(m_rt_timer_id);                  /**< Connection parameters timer. */

#ifdef ENABLE_PEDOMETER
APP_TIMER_DEF(m_pedo_timer_id);                  /**< Connection parameters timer. */
#endif //ENABLE_PEDOMETER

#ifdef ENABLE_FAVORITE_MANAGER
APP_TIMER_DEF(m_favo_timeout_timer_id);
APP_TIMER_DEF(m_favo_log_timer_id);
#endif //ENABLE_FAVORITE_MANAGER

#ifdef ENABLE_MCU_TIMEOUT
APP_TIMER_DEF(m_mcu_timeout_timer_id);
#endif //ENABLE_MCU_TIMEOUT

#ifdef ENABLE_PWM
/**
 * @brief Handler for timer events.
 */
void timer_pattern_event_handler(void* p_context)
{

	if( p_pattern_func != NULL )
	{
		p_pattern_func();

		if( g_str_timer.ui16_pattern_cnt == 0 )
		{
			p_pattern_func = NULL;
		}
		else
		{
			g_str_timer.ui16_pattern_cnt--;
		}
	}
	else
	{
		pwm_disable();
		pattern_command_check();
#ifdef ENABLE_DND
		if( g_str_app_status.ui8_dnd_status == DISABLE )
#endif //ENABLE_DND
		{
			g_str_bit.b1_haptic_status = 1;
		}
	}
}

void pattern_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_stop(m_pattern_timer_id);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_start(m_pattern_timer_id, APP_TIMER_TICKS(33, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}


void pattern_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_pattern_timer_id);
	APP_ERROR_CHECK(err_code);
}

void pattern_timer_init(void)
{
    uint32_t err_code = NRF_SUCCESS;

    /* Define a timer id used for 10Hz sample rate */
    err_code = app_timer_create(&m_pattern_timer_id, APP_TIMER_MODE_REPEATED, timer_pattern_event_handler);
    APP_ERROR_CHECK(err_code);
}


void timer_sub_pattern_event_handler(void* p_context)
{

	if( p_sub_pattern_func != NULL )
	{
		p_sub_pattern_func();

		if( g_str_timer.ui16_sub_pattern_cnt == 0 )
		{
			p_sub_pattern_func = NULL;
		}
		else
		{
			g_str_timer.ui16_sub_pattern_cnt--;
		}
	}
	else
	{
		sub_pattern_timer_stop();
		sub_pattern_command_check();
	}
}

void sub_pattern_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_stop(m_sub_pattern_timer_id);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_start(m_sub_pattern_timer_id, APP_TIMER_TICKS(33, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}


void sub_pattern_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_sub_pattern_timer_id);
	APP_ERROR_CHECK(err_code);
}

void sub_pattern_timer_init(void)
{
    uint32_t err_code = NRF_SUCCESS;

    /* Define a timer id used for 10Hz sample rate */
    err_code = app_timer_create(&m_sub_pattern_timer_id, APP_TIMER_MODE_REPEATED, timer_sub_pattern_event_handler);
    APP_ERROR_CHECK(err_code);
}
#endif //ENABLE_PWM

void timer_rt_event_handler(void* p_context)
{
#ifdef ENABLE_PEDOMETER

	if( g_str_time.ui32_date )
	{
		uint32_t cur_time = (g_str_time.ui32_hour&0xFF)<<8;
		cur_time |= (g_str_time.ui8_minute&0xFF);
		if( ( g_str_app_status.ui8_longsit_status ) &&
		   ( g_str_app_status.ui16_longsit_start_time > cur_time ) &&
		   ( g_str_app_status.ui16_longsit_end_time < cur_time ) )
		{
			if( ++g_str_timer.ui16_stay_hour == 3600 )
			{
				uint8_t byte[3];
				uint8_t nus_index = 0;

				byte[++nus_index] = REPORT_APP_CONFIG;
				byte[++nus_index] = REP_CONFIG_LONGSIT_STATUS;
				byte[0] = ++nus_index;
				nus_tx_push(byte, byte[0]);
				
				g_str_timer.ui16_stay_hour = 0;
		#ifdef ENABLE_PWM
				call_long_sit_noti();
		#endif
			}
		}
#ifdef ENABLE_CTS
		if( ++g_str_time.ui8_second > 59 )
		{
			if( g_str_time.ui8_minute < 59 )
			{
				g_str_time.ui8_minute++;
			}
			else
			{
				pedo_flash_hour_write();
				g_str_time.ui8_minute = 0;
				time_update();
			}
			g_str_time.ui8_second = 0;
		}
#else
		if( ++g_str_timer.ui16_pedo_hour == 3600 )
		{
			pedo_flash_hour_write();
			
			g_str_timer.ui16_pedo_hour = 0;
			
			time_update();
		}

#endif

	}
#else //ifndef ENABLE_PEDOMETER
	if( g_str_time.ui32_date )
	{
		if( ++g_str_time.ui8_second == 60 )
		{
			if( g_str_time.ui8_minute < 59 )
			{
				g_str_time.ui8_minute++;
			}
		}
	}
#endif
	

	if( ( ++g_str_timer.ui16_batt_cnt %60 ) == 2 )
	{
#ifdef ENABLE_ADC
		adc_enable();
#else
	#ifdef ENABLE_FUEL_GAUGE
		fuel_gauge_init();
	#endif
#endif //ENABLE_ADC
		g_str_timer.ui16_batt_cnt = 2;
	}
	
#ifdef ENABLE_POWER_KEY
	if( g_str_bit.b1_power_on_check )
	{
		g_str_timer.ui8_power_on_cnt++;
		
		if( g_str_timer.ui8_power_on_cnt > 2 )
		{
			g_str_bit.b1_power_on = 1;
		}
	}
#endif
	
	if( g_str_bit.b1_ble_send_wait )
	{
		static int wait_cnt = 0;
		
		if( wait_cnt )
		{
			g_str_bit.b1_ble_send_wait = 0;
			wait_cnt = 0;
		}
		else
		{
			wait_cnt++;
		}
	}

	if( g_str_bit.b1_adv_status )
	{
		//disconn status
		if( ++g_str_timer.ui16_disconn_cnt > 600 )
		{
			g_str_timer.ui16_disconn_cnt = 0;
			call_disconn_timeout();
		}
	}

	if( g_str_bit.b1_conn_noti )
	{
		if( ++g_str_timer.ui16_conn_noti_cnt == 5 )
		{
			//conn noti
			g_str_bit.b1_conn_noti = 0;
			g_str_timer.ui16_conn_noti_cnt = 0;
			pattern_pairing_complete();
		}
	}
	
#ifdef ENABLE_CTS	
	if( g_str_bit.b1_cts_ready )
	{
		if (m_cts.conn_handle != BLE_CONN_HANDLE_INVALID)
		{
			ret_code_t ret = ble_cts_c_current_time_read(&m_cts);
			if (ret == NRF_ERROR_NOT_FOUND)
			{
				NRF_LOG_INFO("Current Time Service is not discovered.\r\n");
			}
		}
	}
#endif
}

void monitor_timer_init( void )
{
	uint32_t err_code = NRF_SUCCESS;

	/* Define a timer id used for 10Hz sample rate */
	err_code = app_timer_create(&m_rt_timer_id, APP_TIMER_MODE_REPEATED, timer_rt_event_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_start(m_rt_timer_id, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}

void day_of_week( void )
{
	uint8_t year	= (uint8_t)(g_str_time.ui32_date>>16);
	uint8_t month	= (uint8_t)((g_str_time.ui32_date&0xff00)>>8);
	uint8_t days	= (uint8_t)(g_str_time.ui32_date & 0xff);

	int total = (year - 1) * 365 + (((year-1) / 4) - ((year-1) / 100) + ((year-1) / 400));
	int m[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};		// last day of month
	m[1] =  year % 4 == 0 && year % 100 != 0 || year % 400 == 0 ? 29 : 28;	// check leap year

	for(int i=1; i <month; i++)
	{
		total += m[i-1];

	}
	total += days;
	
	g_str_time.ui8_last_day_of_week = g_str_time.ui8_day_of_week;
	g_str_time.ui8_day_of_week = total % 7;
	
	if( g_str_time.ui8_last_day_of_week )
	{
		if( g_str_time.ui8_day_of_week == 0 )
		{
#ifdef ENABLE_FAVORITE_MANAGER
			//favo log initailize
			favorite_log_init();
#endif
		}
	}
	
	if( !g_str_bit.b1_por_status )
	{
#ifdef ENABLE_PEDOMETER
		g_ui32_pedometer_times = 0;
		memset(&g_str_pedo, 0, sizeof(pedometer_struct_t));
		app_status_read();
		reset_steps();
#endif
	}
	
	g_str_bit.b1_daily_step_noti = 1;
	g_str_bit.b1_daily_active_noti = 1;
}

void time_update( void )
{
	uint32_t year = 0;
	uint32_t month = 0;
	uint32_t days = 0;

	if(++g_str_time.ui32_hour >= 24)// When it is 24 o'clock
	{       
		
		g_str_time.ui32_hour = g_str_time.ui32_hour % 24;
		year = (g_str_time.ui32_date&0xff0000)>>16;
		month = (g_str_time.ui32_date&0xff00)>>8;
		days = (g_str_time.ui32_date & 0xff);

		if( ( (month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12) ) && (days == 31) )
		{
			month++;
			days = 1;
			if(month == 13)
			{
				year++;
				month = 1;
			}
		}
		else if( ( (month == 4) || (month == 6) || (month == 9) || (month == 11) ) && (days == 30) )
		{
			month++;
			days = 1;
		}
		else if( (month == 2) && ( ( ( (year % 4) == 0 ) && (days == 29) ) || ( ( (year % 4) != 0 ) && (days == 28) ) ) )//leap year
		{
			month++;
			days = 1;
		}
		else
		{
			days++;
		}
#ifdef ENABLE_PEDOMETER
		if( g_ui32_flash_hour_write_cnt )
		{
			pedo_flash_daily_write();
		}
#endif				
		g_str_time.ui32_date = (year<<16) | (month<<8) | days;
		day_of_week();
	}
	
}

#ifdef ENABLE_PEDOMETER

void timer_pedo_event_handler(void* p_context)
{
	static uint32_t ui32_last_step = 0;
	static uint16_t ui16_err_cnt = 0;

	if( g_str_bit.b2_pedo_mode )
	{
		if( g_str_bit.b2_pedo_mode == 1 )
		{
			g_str_pedo.ui16_total_walk_time++;
		}
		else if ( g_str_bit.b2_pedo_mode == 2 )
		{
			g_str_pedo.ui16_total_run_time++;
		}

		g_ui32_pedometer_times++;
	}

	if( g_str_app_status.ui8_activity_status )
	{
		if( g_str_bit.b1_daily_active_noti )
		{
			if( g_ui32_pedometer_times > ( g_str_app_status.ui16_target_active_time * 60 ) )
			{
				// noti times
				if( ( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING ) || ( g_str_call_bit.b1_favorite_enable ) )
				{
					if( !g_str_app_status.ui8_dnd_status )
					{
						call_sub_activity_noti();
					}
				}
				else
				{
					call_activity_noti();
				}
			}
			g_str_bit.b1_daily_active_noti = 0;
		}
	}	

	if( ui32_last_step == g_ui32_pedometer_steps )
	{
		ui16_err_cnt++;
	}
	else
	{
		ui16_err_cnt = 0;
	}
	
	if( ui16_err_cnt > 5 )
	{
		if( g_str_bit.b2_pedo_mode )
		{
			pedo_timer_stop();
		}
		g_str_bit.b2_pedo_mode = 0;
	}
	
	ui32_last_step = g_ui32_pedometer_steps;
}

void pedo_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_pedo_timer_id);
	APP_ERROR_CHECK(err_code);
}

void pedo_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_start(m_pedo_timer_id, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}

void pedo_timer_init( void )
{
	uint32_t err_code = NRF_SUCCESS;

	/* Define a timer id used for 10Hz sample rate */
	err_code = app_timer_create(&m_pedo_timer_id, APP_TIMER_MODE_REPEATED, timer_pedo_event_handler);
	APP_ERROR_CHECK(err_code);

}

#endif //ENABLE_PEDOMETER

#ifndef ENABLE_ADC
void timer_charger_event_handler(void* p_context)
{
	if( ++g_str_timer.ui8_charge_cnt == 2 )
	{
		if(g_str_bit.b1_power_connection_chk)//connected
		{
			if(g_str_bit.b1_chg_status )//charging
			{
				g_str_batt.ui16_batt_status = BATT_STATUS_IN_CHARGING;
				
				if(g_str_batt.ui8_charger_status > 2)//charging error
				{
					g_str_batt.ui16_batt_status = BATT_STATUS_INITIAL;//charger error
				}
			}
			else//discharging
			{
				if(g_str_batt.ui16_percent >= 100)
				{
					g_str_batt.ui16_batt_status = BATT_STATUS_CHARGING_COMPLETE;
				}
			}
		}
		else
		{
			charger_timer_stop();
			g_str_batt.ui8_charger_status = 0;
			g_str_timer.ui8_charge_cnt = 0;
		}

		g_str_batt.ui8_charger_status = 0;
		g_str_timer.ui8_charge_cnt = 0;
	}
}
void charger_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_charger_timer_id);
	APP_ERROR_CHECK(err_code);
}
void charger_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_charger_timer_id);
	APP_ERROR_CHECK(err_code);
	err_code = app_timer_start(m_charger_timer_id, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}
void charger_timer_init( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_create(&m_charger_timer_id, APP_TIMER_MODE_REPEATED, timer_charger_event_handler);
	APP_ERROR_CHECK(err_code);
	charger_timer_start();
}

#endif //ifndef ENABLE_ADC

#ifdef ENABLE_FAVORITE_MANAGER

/**@brief Function for handling the security request timer time-out.
 *
 * @details This function is called each time the security request timer expires.
 *
 * @param[in] p_context  Pointer used for passing context information from the
 *                       app_start_timer() call to the time-out handler.
 */
static void favo_timeout_handler(void * p_context)
{
	uint32_t err_code;

	g_str_call_bit.b1_favorite_enable = 0;
#ifdef HEEJIN_DBG
	g_str_call_bit.b1_wkup_flag = 0;
#endif
	mcu_wkup_off();
	
	err_code = app_timer_stop(m_favo_timeout_timer_id);
	APP_ERROR_CHECK(err_code);
}

void favo_timeout_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_favo_timeout_timer_id);
	APP_ERROR_CHECK(err_code);
}

void favo_timeout_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_start(m_favo_timeout_timer_id, APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}

static void favo_log_timer_handler(void * p_context)
{
	if( g_str_call_bit.b1_call_inout )
	{
		g_str_favo_log.ui32_outgo_time++;
	}
	else
	{
		g_str_favo_log.ui32_incom_time++;
	}
}

void favo_log_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_favo_log_timer_id);
	APP_ERROR_CHECK(err_code);
}

void favo_log_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_start(m_favo_log_timer_id, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}

void favo_timer_init( void )
{
	ret_code_t ret;

	ret = app_timer_create(&m_favo_timeout_timer_id,
                           APP_TIMER_MODE_SINGLE_SHOT,
                           favo_timeout_handler);

    APP_ERROR_CHECK(ret);
	
	ret = app_timer_create(&m_favo_log_timer_id,
                           APP_TIMER_MODE_REPEATED,
                           favo_log_timer_handler);

    APP_ERROR_CHECK(ret);

}

#endif //ENABLE_FAVORITE_MANAGER

#ifdef ENABLE_MCU_TIMEOUT

static void mcu_timeout_timer_handler(void * p_context)
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_stop(m_mcu_timeout_timer_id);
	APP_ERROR_CHECK(err_code);
	
	uart_tx_reset();

	if( g_str_timeout.b1_pairing_on )
	{
		if( --g_str_timeout.b3_timeout_retry )
		{
			mcu_timeout_timer_start();
			g_str_timeout.b1_pairing_on = 1;
			bt_pairing_on();
		}
		else
		{
			g_str_timeout.b1_pairing_on = 0;
			mcu_wkup_off();
			uart_close();
		}
	}
	else if( g_str_timeout.b1_receive_call )
	{
		//call receive fail notify
		g_str_timeout.b1_receive_call = 0;
	}
	else if( g_str_timeout.b1_end_call )
	{
		if( --g_str_timeout.b3_timeout_retry )
		{
			mcu_timeout_timer_start();
			bt_end_call();
		}
		else
		{
			//mcu_reset
			g_str_call_bit.b4_call_status = CALL_STATUS_READY;
			g_str_timeout.b1_end_call = 0;
			mcu_wkup_off();
			uart_close();
		}
	}
	else if( g_str_timeout.b1_make_call )
	{
		if( --g_str_timeout.b3_timeout_retry )
		{
			mcu_timeout_timer_start();
			bt_make_call();
		}
		else
		{
			//call make call fail notify
			g_str_call_bit.b4_call_status &= ~(CALL_STATUS_FAVORITE);
			g_str_timeout.b1_make_call = 0;
			mcu_wkup_off();
			uart_close();
		}
	}
	else if( g_str_timeout.b1_volume_control )
	{
		g_str_timeout.b1_volume_control = 0;
	}
	else if( g_str_timeout.b1_factory_reset )
	{
		g_str_timeout.b1_factory_reset = 0;
		mcu_wkup_off();
		uart_close();
	}
	
}

void mcu_timeout_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	
	g_str_timeout.b3_timeout_retry = 0;	
	err_code = app_timer_stop(m_mcu_timeout_timer_id);
	APP_ERROR_CHECK(err_code);
}

void mcu_timeout_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;
	
	err_code = app_timer_start(m_mcu_timeout_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}

void mcu_timeout_init( void )
{
	ret_code_t ret;

	ret = app_timer_create(&m_mcu_timeout_timer_id,
                           APP_TIMER_MODE_SINGLE_SHOT,
                           mcu_timeout_timer_handler);

    APP_ERROR_CHECK(ret);
}
#endif //ENABLE_MCU_TIMEOUT

void user_timers_init( void )
{
	monitor_timer_init();
#ifdef ENABLE_PWM
	pattern_timer_init();
	sub_pattern_timer_init();
#endif
#ifdef ENABLE_PEDOMETER
	pedo_timer_init();
#endif
#ifdef ENABLE_FAVORITE_MANAGER
	favo_timer_init();
#endif
	
#ifdef ENABLE_MCU_TIMEOUT
	mcu_timeout_init();
#endif
	
#ifndef ENABLE_ADC
	charger_timer_init();
#endif
}
