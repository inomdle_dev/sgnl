/**
 * File name : mcu_protocol.c
 *
 * This file contains the source code for parsing uart serial data (BLE <-> MCU)
 */

#include "peripheral_define.h"

#ifdef ENABLE_UART

#define TMP_TEST
#ifdef TMP_TEST
	bsp_event_t tmp_protocol[4] = { BSP_EVENT_NOTHING, BSP_EVENT_KEY_2, BSP_EVENT_KEY_1, BSP_EVENT_KEY_0 };
#endif

enum {
	BT_OFF		= 0x00,
	BT_ON		= 0x01,
} ;

enum {
	PAIR_START			= 0x01,
	PAIR_TIMEOUT		= 0x02,
	PAIR_SUCCESS		= 0x03,
	PAIR_FAIL			= 0x04,
} ;

enum {
	MCU_SLEEP			= 0x00,
	MCU_READY			= 0x01,
} ;

enum {
	CALL_OUTGOING		= 0x01,
	CALL_CALLING		= 0x02,
	CALL_END_CALL		= 0x03,
} ;

enum {
	MCU_REPORT_BT_STATUS			= 0x01,
	MCU_REPORT_PAIRING_STATUS		= 0x02,
	MCU_REPORT_MCU_STATUS			= 0x03,
	MCU_REPORT_CALL_STATUS			= 0x04,
	MCU_REPORT_FACTORY_RESET		= 0x05,
	
#ifdef TMP_TEST
	MCU_REPORT_BUTTON_STATUS		= 0x07,
#endif
};

enum {
	SEND_BT_PAIRING_ON			= 0x01,
	SEND_CALL_STATUS			= 0x02,
	SEND_VOL_CONTROL			= 0x03,
	SEND_FACTORY_RESET			= 0x04,
	SEND_MCU_GO_SLEEP			= 0x05,
};

enum {
	SEND_INCOMMING_CALL			= 0x01,
	SEND_RECEIVE_CALL			= 0x02,
	SEND_END_CALL				= 0x03,
	SEND_MAKE_CALL				= 0x04,
	SEND_OUTGOING_CALL			= 0x05,
	SEND_REJECT_CALL			= 0x06,
};

enum {
	SEND_VOL_UP					= 0x01,
	SEND_VOL_DOWN				= 0x02,
};

void mcu_go_sleep( void )
{
	uint8_t byte[2];
	byte[0] = 2;						//length
	byte[1] = SEND_MCU_GO_SLEEP;	//Command ID
	
	mcu_wkup_on();

	uart_tx_push(byte, byte[0]);
}

void bt_pairing_on( void )
{
	uint8_t byte[2];

	byte[0] = 2;					//length
	byte[1] = SEND_BT_PAIRING_ON;	//Command ID

	mcu_wkup_on();	

	uart_tx_push(byte, byte[0]);
}

void bt_incoming_call( void )
{
	uint8_t byte[3];
	byte[0] = 3;					//length
	byte[1] = SEND_CALL_STATUS;		//Command ID
	byte[2] = SEND_INCOMMING_CALL;	//type
	
	mcu_wkup_on();	

	uart_tx_push(byte, byte[0]);
}
void bt_receive_call( void )
{
	uint8_t byte[3];
	byte[0] = 3;					//length
	byte[1] = SEND_CALL_STATUS;		//Command ID
	byte[2] = SEND_RECEIVE_CALL;	//type
	
#ifdef HEEJIN_DBG
#else
	mcu_wkup_on();
#endif

	uart_tx_push(byte, byte[0]);
}

void bt_end_call( void )
{
	uint8_t byte[3];
	byte[0] = 3;					//length
	byte[1] = SEND_CALL_STATUS;		//Command ID
	byte[2] = SEND_END_CALL;	//type

	mcu_wkup_off();	

	uart_tx_push(byte, byte[0]);
}

void bt_reject_call( void )
{
	uint8_t byte[3];
	byte[0] = 3;					//length
	byte[1] = SEND_CALL_STATUS;		//Command ID
	byte[2] = SEND_REJECT_CALL;	//type

	mcu_wkup_off();	

	uart_tx_push(byte, byte[0]);
}

void bt_outgoing_call( void )
{
	uint8_t byte[3];
	byte[0] = 3;	//length
	byte[1] = SEND_CALL_STATUS;					//Command ID
	byte[2] = SEND_OUTGOING_CALL;					//type
	
#ifdef HEEJIN_DBG
	mcu_wkup_on();
#endif

	uart_tx_push(byte, byte[0]);
#ifdef HEEJIN_DBG
	mcu_wkup_off();
#endif
}

void bt_make_call( void )
{
	uint8_t byte[20] = "";
	byte[0] = g_str_favo_mgr.ui8_num_len + 3;	//length
	byte[1] = SEND_CALL_STATUS;					//Command ID
	byte[2] = SEND_MAKE_CALL;					//type
	strncpy((char *)&byte[3], (char *)g_str_favo_mgr.ui8_number, g_str_favo_mgr.ui8_num_len );	
	
	mcu_wkup_on();	

	uart_tx_push(byte, byte[0]);
}

void bt_volume_up( void )
{
	uint8_t byte[3];
	byte[0] = 3;					//length
	byte[1] = SEND_VOL_CONTROL;		//Command ID
	byte[2] = SEND_VOL_UP;			//type
	
	uart_tx_push(byte, byte[0]);
}

void bt_volume_down( void )
{
	uint8_t byte[3];
	byte[0] = 3;					//length
	byte[1] = SEND_VOL_CONTROL;		//Command ID
	byte[2] = SEND_VOL_DOWN;		//type
	
	uart_tx_push(byte, byte[0]);
}

void bt_factory_reset( void )
{
	uint8_t byte[2];
	byte[0] = 2;					//length
	byte[1] = SEND_FACTORY_RESET;	//Command ID

	mcu_wkup_on();

	uart_tx_push(byte, byte[0]);
}

void mcu_protocol(uint8_t* u8_data, uint8_t u8_size)
{
	uint8_t u8_data_type = u8_data[1];
	
	switch( u8_data_type )
	{
	case MCU_REPORT_BT_STATUS :
		switch(u8_data[2])
		{
		case BT_OFF :
			break;
		
		case BT_ON :
			break;

		default : break;
		}  
		break;

	case MCU_REPORT_PAIRING_STATUS :
		switch(u8_data[2])
		{
		case PAIR_START :
#ifdef ENABLE_MCU_TIMEOUT
			mcu_timeout_timer_stop();
			g_str_timeout.b1_pairing_on = 0;
#endif
			break;
		
		case PAIR_TIMEOUT :
			break;
		
		case PAIR_SUCCESS :
			g_str_app_status.ui8_audio_bt_paired = 1;
			app_status_write();
			break;
		
		case PAIR_FAIL :
			break;
		
		default : break;
		}  
		break;

	case MCU_REPORT_MCU_STATUS :
		switch(u8_data[2])
		{
		case MCU_SLEEP :
			g_str_bit.b1_mcu_status = 0;
			
			uart_close();
			break;
		
		case MCU_READY :
			g_str_bit.b1_mcu_status = 1;
			mcu_wkup_off();
			
			break;

		default : break;
		}  
		break;

	case MCU_REPORT_CALL_STATUS :
		switch(u8_data[2])
		{
		case CALL_OUTGOING :
#ifdef ENABLE_MCU_TIMEOUT
			mcu_timeout_timer_stop();
			g_str_timeout.b1_make_call = 0;
#endif
			
			g_str_call_bit.b4_call_status |= CALL_STATUS_OUTGOING;
			if( g_str_call_bit.b4_call_status & CALL_STATUS_FAVORITE )
			{
				call_favorite_call();
			}
			break;
		
		case CALL_CALLING :
#ifdef ENABLE_MCU_TIMEOUT
			mcu_timeout_timer_stop();
			g_str_timeout.b1_receive_call = 0;
#endif			
			if( g_str_call_bit.b4_call_status & CALL_STATUS_OUTGOING )
			{
				g_str_call_bit.b4_call_status &= ~(CALL_STATUS_OUTGOING);
				
				if( g_str_call_bit.b4_call_status & CALL_STATUS_FAVORITE )
				{
					{
						g_str_favo_log.ui16_outgo_cnt++;
						favorite_log_write(g_str_favo_log.ui8_index);
						g_str_call_bit.b1_call_inout = 1;
						favo_log_timer_start();
					}
				}
			}
			else if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
			{
				g_str_call_bit.b4_call_status &= ~(CALL_STATUS_INCOMING);

				if( g_str_call_bit.b4_call_status & CALL_STATUS_FAVORITE )
				{
					g_str_favo_log.ui16_incom_cnt++;
					favorite_log_write(g_str_favo_log.ui8_index);
					g_str_call_bit.b1_call_inout = 0;
					favo_log_timer_start();
				}
			}

			g_str_call_bit.b4_call_status |= CALL_STATUS_CALLING;
			break;

		case CALL_END_CALL :
#ifdef ENABLE_MCU_TIMEOUT
			mcu_timeout_timer_stop();
			g_str_timeout.b1_end_call = 0;
#endif
#ifdef ENABLE_FAVORITE_MANAGER
			favorite_log_process();
#endif
			g_str_call_bit.b4_call_status = CALL_STATUS_READY;
			break;
		
		default : break;
		}  
		break;

	case MCU_REPORT_FACTORY_RESET :
#ifdef ENABLE_MCU_TIMEOUT
		g_str_timeout.b1_factory_reset = 0;
#endif
		break;

#ifdef TMP_TEST
	case MCU_REPORT_BUTTON_STATUS :
		button_event(tmp_protocol[u8_data[2]]);
		break;
#endif

	default : break;

	}
}

#endif //ENABLE_UART
