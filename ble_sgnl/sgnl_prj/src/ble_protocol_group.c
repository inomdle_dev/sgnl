/**
* File name : ble_protocol_group.c
*
* This file contains the source code for parsing ble serial data
*/

#include "peripheral_define.h"

/* ble data parser */
void ble_protocol_group(uint8_t* u8_data, uint8_t u8_size)
{
	uint8_t u8_command_id = u8_data[1];
	uint8_t u8_data_type = u8_data[2];
	
#ifdef ENABLE_IFA_DEMO
	if( g_str_batt.ui16_percent == 0 )
	{
		uint8_t byte[5];
		uint8_t nus_index = 0;
		byte[++nus_index] = REPORT_APP_CONFIG;
		byte[++nus_index] = REP_CONFIG_BATT_STATUS;
		byte[++nus_index] = REP_BATT_PERCENTAGE;
		byte[++nus_index] = 0;
		
		byte[0] = ++nus_index;
		nus_tx_push(byte,byte[0]);

		call_batt_low();
		return;
	}
#endif
	
	if( g_str_bit.b1_favorite_save_ready )
	{
		if( !( u8_command_id == REPORT_FAVORITE_DATA ) && ( u8_data_type == REP_FAVO_DATA_SEND ) )
		{
			return;
		}
	}
	
	switch( u8_command_id )
	{
	case REPORT_CALL_STATUS:
		switch( u8_data_type )
		{
		case REP_CALL_INCOMING:
			g_str_call_bit.b4_call_status = CALL_STATUS_INCOMING;
#ifdef ENABLE_PWM
			call_incoming_call();
#endif
#ifdef ENABLE_BT_CONTROL
			bt_incoming_call();
#endif
			break;

#ifdef ENABLE_FAVORITE_MANAGER
		case REP_CALL_FAVORITE_INCOME:
			g_str_favo_mgr.ui8_index = u8_data[3];
			g_str_call_bit.b4_call_status = CALL_STATUS_FAVORITE_INCOMING;
#ifdef ENABLE_PWM
			call_incoming_favorite();
#endif
#ifdef ENABLE_BT_CONTROL
			bt_incoming_call();
#endif
			break;
#endif //ENABLE_FAVORITE_MANAGER

		case REP_CALL_RECEIVED:
#ifdef ENABLE_PWM
			force_off_pattern();
#endif
#ifdef ENABLE_BT_CONTROL
			bt_receive_call();
#endif
			break;
		case REP_CALL_END:
			if( g_str_call_bit.b4_call_status == CALL_STATUS_INCOMING )
			{
				g_str_call_bit.b4_call_status = CALL_STATUS_READY;
			}
#ifdef ENABLE_PWM
			force_off_pattern();
#endif
#ifdef ENABLE_BT_CONTROL
			bt_end_call();
#endif
			break;
			
		default :
			break;
		}
		break;
		
	case REPORT_NOTIFICATION:
		switch( u8_data_type )
		{
		case REP_NOTI_APP_NOTI:
#ifdef ENABLE_PWM
			if( g_str_app_status.ui8_app_noti_status )
			{
				if( ( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING ) || ( g_str_call_bit.b1_favorite_enable ) )
				{
					if( !g_str_app_status.ui8_dnd_status )
					{
						call_sub_app_noti();
					}
				}
				else
				{
					call_app_noti();
				}
			}
#endif
			break;
			
		case REP_NOTI_STEP_NOTI:
#ifdef ENABLE_PWM
			if( g_str_app_status.ui8_step_status )
			{
				if( ( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING ) || ( g_str_call_bit.b1_favorite_enable ) )
				{
					if( !g_str_app_status.ui8_dnd_status )
					{
						call_sub_activity_noti();
					}
				}
				else
				{
					call_activity_noti();
				}
			}
#endif
			break;
		case REP_NOTI_ACT_NOTI:
#ifdef ENABLE_PWM
			if( g_str_app_status.ui8_activity_status )
			{
				if( ( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING ) || ( g_str_call_bit.b1_favorite_enable ) )
				{
					if( !g_str_app_status.ui8_dnd_status )
					{
						call_sub_activity_noti();
					}
				}
				else
				{
					call_activity_noti();
				}
			}
#endif
			break;
			
		default :
			break;
		}
		break;
		
	case REPORT_PEDOMETER:
		switch( u8_data_type )
		{
#ifdef ENABLE_PEDOMETER
		case REP_PEDO_LIVE_DATA :
			{
				uint32_t ui32_cur_time, ui32_new_time;
				uint32_t ui32_tmp_date;
				
				g_str_time.ui32_app_date = (uint32_t)( (u8_data[3]<<16) | (u8_data[4]<<8) | u8_data[5] );
				g_str_time.ui32_app_hour = (uint32_t)u8_data[6];

				uint8_t u8_live_send = 0;
				
				uint8_t byte[20] = "";
				uint8_t nus_index = 0;
				uint16_t walktime = 0;
				uint16_t runtime = 0;

				uint32_t walk = 0;
				uint32_t run = 0;

				ui32_cur_time  = ( uint32_t )( ( g_str_time.ui32_date & 0x00FFFFFF ) << 8 );
				ui32_new_time  = ( uint32_t )( ( g_str_time.ui32_app_date & 0x00FFFFFF ) << 8 );
				
				ui32_cur_time |= ( uint32_t )( g_str_time.ui32_hour & 0x00ff );
				ui32_new_time |= ( uint32_t )( g_str_time.ui32_app_hour & 0x00ff );
				
				// check time
				if( ui32_new_time > ui32_cur_time )
				{
					g_str_time.ui8_second = 0;
					// last time data send
					if( g_str_time.ui32_date != 0 )
					{
						walktime = g_str_pedo.ui16_total_walk_time;
						uint16_t runtime = g_str_pedo.ui16_total_run_time;

						uint32_t walk = g_str_pedo.ui32_total_walk_step;
						uint32_t run = g_str_pedo.ui32_total_run_step;

						byte[++nus_index] = REPORT_PEDOMETER;
						byte[++nus_index] = REP_PEDO_PAST_TIME;
						byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_date & 0xff0000 ) >> 16 );
						byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_date & 0xff00 ) >> 8 );
						byte[++nus_index] = ( uint8_t )( g_str_time.ui32_date & 0xff );
						byte[++nus_index] = ( uint8_t )( g_str_time.ui32_hour );

						byte[++nus_index] = ( uint8_t )((walk&0xff000000)>>24);
						byte[++nus_index] = ( uint8_t )((walk&0x00ff0000)>>16);
						byte[++nus_index] = ( uint8_t )((walk&0x0000ff00)>>8);
						byte[++nus_index] = ( uint8_t )(walk&0x000000ff);
						byte[++nus_index] = ( uint8_t )((walktime&0xff00)>>8);
						byte[++nus_index] = ( uint8_t )(walktime&0x00ff);
						byte[++nus_index] = ( uint8_t )((run&0xff000000)>>24);
						byte[++nus_index] = ( uint8_t )((run&0x00ff0000)>>16);
						byte[++nus_index] = ( uint8_t )((run&0x0000ff00)>>8);
						byte[++nus_index] = ( uint8_t )(run&0x000000ff);
						byte[++nus_index] = ( uint8_t )((runtime&0xff00)>>8);
						byte[++nus_index] = ( uint8_t )(runtime&0x00ff);

						byte[0] = ++nus_index;
						nus_tx_push(byte, byte[0]);
						
						pedo_flash_hour_write();
					}
					else
					{
						u8_live_send = 1;
						
						walktime = g_str_pedo.ui16_total_walk_time;
						runtime = g_str_pedo.ui16_total_run_time;

						walk = g_str_pedo.ui32_total_walk_step;
						run = g_str_pedo.ui32_total_run_step;
					}
				}
				// time update
				ui32_tmp_date = g_str_time.ui32_app_date;
				if( ui32_tmp_date != g_str_time.ui32_date )
				{
					if( g_str_time.ui32_date )
					{
						pedo_flash_daily_write();
					}
					g_str_time.ui32_date = ui32_tmp_date;
					day_of_week();
				}
				g_str_time.ui32_hour = g_str_time.ui32_app_hour;
				
				if( g_str_bit.b1_pedo_err )
				{
					uint8_t data[3];
					uint8_t ui8_index = 0;
					data[++ui8_index] = REPORT_ERR_MSG;
					data[++ui8_index] = REP_ERR_PEDO_INIT;
					data[0] = ++ui8_index;
					
					nus_tx_push(data,data[0]);
					if( init_motion_sensor_driver() )
					{
						// pedo error
						g_str_bit.b1_pedo_err = 1;
						u8_live_send = 1;
						
						walktime = 0;
						runtime = 0;

						walk = 0;
						run = 0;
					}
					else
					{
						g_str_bit.b1_pedo_err = 0;
						g_str_bit.b1_pedo_transmit = 1;
					}
				}
				else
				{
					g_str_bit.b1_pedo_transmit = 1;
				}
				
				if( u8_live_send )
				{
					byte[++nus_index] = REPORT_PEDOMETER;
					byte[++nus_index] = REP_PEDO_LIVE_DATA;
					byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff0000 ) >> 16 );
					byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff00 ) >> 8 );
					byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_date & 0xff );
					byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_hour );

					byte[++nus_index] = ( uint8_t )((walk&0xff000000)>>24);
					byte[++nus_index] = ( uint8_t )((walk&0x00ff0000)>>16);
					byte[++nus_index] = ( uint8_t )((walk&0x0000ff00)>>8);
					byte[++nus_index] = ( uint8_t )(walk&0x000000ff);
					byte[++nus_index] = ( uint8_t )((walktime&0xff00)>>8);
					byte[++nus_index] = ( uint8_t )(walktime&0x00ff);
					byte[++nus_index] = ( uint8_t )((run&0xff000000)>>24);
					byte[++nus_index] = ( uint8_t )((run&0x00ff0000)>>16);
					byte[++nus_index] = ( uint8_t )((run&0x0000ff00)>>8);
					byte[++nus_index] = ( uint8_t )(run&0x000000ff);
					byte[++nus_index] = ( uint8_t )((runtime&0xff00)>>8);
					byte[++nus_index] = ( uint8_t )(runtime&0x00ff);

					byte[0] = ++nus_index;
					nus_tx_push(byte, byte[0]);
				}
			}
			
			break;
		case REP_PEDO_PAST_TIME :
			g_str_time.ui32_app_date = (uint32_t)( (u8_data[3]<<16) | (u8_data[4]<<8) | u8_data[5] );
			g_str_time.ui32_app_hour = (uint32_t)u8_data[6];
			
			pedo_flash_hour_read();
			
			break;		
			
		case REP_PEDO_PAST_DATE :
			g_str_time.ui32_app_date = (uint32_t)( (u8_data[3]<<16) | (u8_data[4]<<8) | u8_data[5] );
			
			pedo_flash_daily_read();
			
			break;
			
#endif //ENABLE_PEDOMETER
		default :
			break;
		}
		break;
		
	case REPORT_APP_CONFIG:
		{
			uint8_t u8_status = u8_data[3];
			switch( u8_data_type )
			{
			case REP_CONFIG_APP_NOTI_STATUS:
				g_str_app_status.ui8_app_noti_status = u8_status;
				app_status_write();
				break;
			
			case REP_CONFIG_DND_STATUS:
				g_str_app_status.ui8_dnd_status = u8_status;
				app_status_write();
				break;
			
			case REP_CONFIG_LONGSIT_STATUS:
				{
					uint8_t u8_param = u8_data[4];
					switch(u8_status)
					{
					case REP_LONGSIT_CONFIG:
						g_str_app_status.ui8_longsit_status = u8_param;
						app_status_write();
						break;
					case REP_LONGSIT_START:
						g_str_app_status.ui16_longsit_start_time = u8_data[4]<<8;
						g_str_app_status.ui16_longsit_start_time |= u8_data[5];
						app_status_write();
						break;
					case REP_LONGSIT_STOP:
						g_str_app_status.ui16_longsit_end_time = u8_data[4]<<8;
						g_str_app_status.ui16_longsit_end_time |= u8_data[5];
						app_status_write();
						break;
					}
				}
				break;
			
			case REP_CONFIG_STEP_STATUS:
				g_str_app_status.ui8_step_status = u8_status;
				g_str_bit.b1_daily_step_noti = 1;
				app_status_write();
				break;
			
			case REP_CONFIG_STEP_CONFIG:
				g_str_app_status.ui16_target_steps = u8_status;
				g_str_bit.b1_daily_step_noti = 1;
				app_status_write();
				break;
			
			case REP_CONFIG_ACT_STATUS:
				g_str_app_status.ui8_activity_status = u8_status;
				g_str_bit.b1_daily_active_noti = 1;
				app_status_write();
				break;
			
			case REP_CONFIG_ACT_CONFIG:
				if( u8_data[4] )
				{
					g_str_app_status.ui16_target_active_time = u8_data[3]<<8;
					g_str_app_status.ui16_target_active_time |= u8_data[4];
				}
				else
				{
					g_str_app_status.ui16_target_active_time = u8_data[3];
				}
				g_str_bit.b1_daily_active_noti = 1;
				app_status_write();
				break;

#ifdef ENABLE_BT_CONTROL			
			case REP_CONFIG_BT_PAIR_REQ:
	#ifdef ENABLE_MCU_TIMEOUT
				mcu_timeout_timer_start();
				g_str_timeout.b1_pairing_on = 1;
	#endif
				bt_pairing_on();
				break;
#endif //ENABLE_BT_CONTROL

	#ifdef ENABLE_BATT		
			case REP_CONFIG_BATT_STATUS:
				{
					uint8_t byte[5] = "";
					uint8_t nus_index = 0;
					
					byte[++nus_index] = REPORT_APP_CONFIG;
					byte[++nus_index] = REP_CONFIG_BATT_STATUS;
					byte[++nus_index] = REP_BATT_PERCENTAGE;
					byte[++nus_index] = g_str_batt.ui16_percent;
					
					byte[0] = ++nus_index;
					nus_tx_push(byte, byte[0]);

					nus_index = 0;
					
					byte[++nus_index] = REPORT_APP_CONFIG;
					byte[++nus_index] = REP_CONFIG_BATT_STATUS;
					byte[++nus_index] = REP_BATT_CHARGER;
					byte[++nus_index] = g_str_batt.ui16_batt_status;
					
					byte[0] = ++nus_index;
					nus_tx_push(byte, byte[0]);
				
		#ifdef ENABLE_PWM
					//call_batt_gauge();
		#endif
				}
	
				break;
	#endif				
			case REP_CONFIG_BLE_READY :
				{
					g_str_time.ui32_date = (u8_data[3]&0xFF)<<16;
					g_str_time.ui32_date |= (u8_data[4]&0xFF)<<8;
					g_str_time.ui32_date |= (u8_data[5]&0xFF);
					g_str_time.ui32_hour = u8_data[6];
					g_str_time.ui8_minute = u8_data[7];
					g_str_time.ui8_second = u8_data[8];
					
					day_of_week();
				}
				break;
				
#ifdef ENABLE_IFA_DEMO
			case REP_CONFIG_BLE_CONN_CHK:
				if( u8_status )//conn
				{
					call_ble_conn_success();
				}
				else
				{
					call_ble_disconn();
				}
				break;
#endif
			default :
				break;
			}
		}
		break;
		
	case REPORT_ANCS_DATA:
		switch( u8_data_type )
		{
		case REP_ANCS_DATA_SEND:
			ancs_data_parcer(&u8_data[3], u8_size-4);
			break;

		default :
			break;
		}
		break;

#ifdef ENABLE_FAVORITE_MANAGER		
	case REPORT_FAVORITE_DATA:
		switch( u8_data_type )
		{
		case REP_FAVO_DATA_SEND:
			g_str_bit.b1_favorite_save_ready = 1;
			favorite_data_parser_group(&u8_data[3], u8_size-4);
			break;

		case REP_FAVO_DATA_SYNC:
			if( g_str_bit.b1_favorite_save_ready )
			{
				return;
			}
			switch(u8_data[3])
			{
			case 1:
				favorite_data_sync( u8_data[4] );
				break;
			
			case 2:
				favorite_data_delete( u8_data[4] );
				break;

			default :
				break;
			}
		default:
			break;
		}
		break;
#endif //ENABLE_FAVORITE_MANAGER		
	default : break;
	
	}
}
