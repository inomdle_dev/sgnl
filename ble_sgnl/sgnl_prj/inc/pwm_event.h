#ifndef _PWM_EVENT_H_
#define _PWM_EVENT_H_

extern void pwm_enable(void);
extern void pwm_disable(void);
extern void pwm_off(void);
extern void pwm_duty_change(void);
extern void pwm_init(void);

extern void pwm_led_1(uint32_t ui32_duty);
extern void pwm_led_2(uint32_t ui32_duty);
extern void pwm_led_3(uint32_t ui32_duty);
extern void pwm_led_4(uint32_t ui32_duty);
extern void pwm_led_5(uint32_t ui32_duty);

#endif //_PWM_EVENT_H_
