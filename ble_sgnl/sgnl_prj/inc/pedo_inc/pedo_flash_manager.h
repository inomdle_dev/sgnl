#ifndef _PEDO_FLASH_MANAGER_H_
#define _PEDO_FLASH_MANAGER_H_

extern void pedo_flash_time_sync( void );

extern void pedo_flash_daily_read( void );
extern void pedo_flash_hour_read( void );
extern void pedo_flash_daily_write( void );
extern void pedo_flash_hour_write( void );

extern uint32_t g_ui32_flash_hour_write_cnt;

#endif //_PEDO_FLASH_MANAGER_H_
