#ifndef _MCU_PROTOCOL_H_
#define _MCU_PROTOCOL_H_

extern void mcu_go_sleep( void );

extern void bt_pairing_on( void );
extern void bt_incoming_call( void );
extern void bt_receive_call( void );
extern void bt_volume_up( void );
extern void bt_volume_down( void );
extern void bt_end_call( void );
extern void bt_reject_call( void );
extern void bt_outgoing_call( void );
extern void bt_make_call( void );
extern void bt_factory_reset( void );

extern void mcu_protocol(uint8_t* u8_data, uint8_t u8_size);

#endif //_MCU_PROTOCOL_H_
