/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#ifndef PIN_SGNL_H
#define PIN_SGNL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"
	
#ifdef ENABLE_ES2_BOARD

	#define LEDS_NUMBER    5

	#define LED_1          7
	#define LED_2          8
	#define LED_3          9
	#define LED_4          10
	#define LED_5          11

	#define LEDS_ACTIVE_STATE 0

	#define LEDS_INV_MASK  LEDS_MASK

	#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5 }

	#define BSP_LED_0      LED_1
	#define BSP_LED_1      LED_2
	#define BSP_LED_2      LED_3
	#define BSP_LED_3      LED_4
	#define BSP_LED_4      LED_5

	#define BUTTONS_NUMBER 3

	#define BUTTON_1       5
	#define BUTTON_2       6
	#define BUTTON_3       3

	#define BUTTON_PULL    NRF_GPIO_PIN_PULLDOWN

	#define BUTTONS_ACTIVE_STATE 1

	#define BUTTONS_LIST { BUTTON_1, BUTTON_2, BUTTON_3 }

	#define BSP_BUTTON_0   BUTTON_1
	#define BSP_BUTTON_1   BUTTON_2
	#define BSP_BUTTON_2   BUTTON_3

	#define ADC_A0_PIN		2     // Analog channel 0
	
	#define POWER_HOLD		4	
	
	#define HAPTIC_OUT		12
	
	#define SCL_PIN			13    // SCL signal pin
	#define SDA_PIN			14    // SDA signal pin

	#define PEDO_INT		15

	#define BLE_TX_PIN		18
	#define BLE_RX_PIN		19
	#define MCU_WKUP		20
	#define HWFC			APP_UART_FLOW_CONTROL_DISABLED
	
	#define BATT_CHK_ON		27
	#define CHK_VBUS		30

#else
	
	#define LEDS_NUMBER    5

	#define LED_1          7
	#define LED_2          8
	#define LED_3          9
	#define LED_4          10
	#define LED_5          11

	#define LEDS_ACTIVE_STATE 0

	#define LEDS_INV_MASK  LEDS_MASK

	#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5 }

	#define BSP_LED_0      LED_1
	#define BSP_LED_1      LED_2
	#define BSP_LED_2      LED_3
	#define BSP_LED_3      LED_4
	#define BSP_LED_4      LED_5

	#define BUTTONS_NUMBER 3

	#define BUTTON_1       5
	#define BUTTON_2       6
	#define BUTTON_3       3

	#define BUTTON_PULL    NRF_GPIO_PIN_PULLDOWN

	#define BUTTONS_ACTIVE_STATE 1

	#define BUTTONS_LIST { BUTTON_1, BUTTON_2, BUTTON_3 }

	#define BSP_BUTTON_0   BUTTON_1
	#define BSP_BUTTON_1   BUTTON_2
	#define BSP_BUTTON_2   BUTTON_3

	#define ADC_A0_PIN		2     // Analog channel 0

	#define POWER_HOLD		4

	#define HAPTIC_OUT		12
	#define PEDO_INT		13

	#define BLE_TX_PIN		18
	#define BLE_RX_PIN		19
	#define MCU_WKUP		20
	#define HWFC			APP_UART_FLOW_CONTROL_DISABLED

	#define SDA_PIN			25    // SDA signal pin
	#define SCL_PIN			26    // SCL signal pin

	#define BATT_CHK_ON     27
	#define CHK_VBUS		30

#endif

// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_RC,            \
                                 .rc_ctiv       = 10,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_250_PPM}

#ifdef __cplusplus
}
#endif

#endif // PIN_SGNL_H
