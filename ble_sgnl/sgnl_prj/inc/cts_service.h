#ifndef _CTS_SERVICE_H_
#define _CTS_SERVICE_H_

#include "ble_cts_c.h"

extern ble_cts_c_t		  m_cts;

void on_cts_c_evt(ble_cts_c_t * p_cts, ble_cts_c_evt_t * p_evt);
void cts_service_init(void);

#endif //_CTS_SERVICE_H_
