#ifndef _FLASH_EVENT_H_
#define _FLASH_EVENT_H_

typedef enum user_flash_list_{
	USER_FLASH_APP_CONFIG = 0,
	USER_FLASH_PEDOMETER_HOUR = 1,
	USER_FLASH_PEDOMETER_DAILY = 2,
	USER_FLASH_PACKAGE_BLOCK = 3,
	USER_FLASH_FAVORITE = 4,
	USER_FLASH_FAVORITE_LOG = 5,
	
	USER_FLASH_LIST = 6,
}user_flash_list_t;

extern void user_str_lower( uint8_t *ui8_data, uint8_t ui8_len );
extern void user_flash_erase( user_flash_list_t list );
extern void user_flash_all_erase(void);
extern void user_flash_write( user_flash_list_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size );
extern void user_flash_read( user_flash_list_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size );

extern void user_flash_init( void );
#endif //_FLASH_EVENT_H_
