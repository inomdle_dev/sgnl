#ifndef _FAVORITE_MANAGER_H_
#define _FAVORITE_MANAGER_H_

extern void favorite_data_parser( uint8_t *u8_data, uint8_t u8_size );
extern void favorite_data_parser_group( uint8_t *u8_data, uint8_t u8_data_len );

extern void favorite_read_number( uint8_t u8_index, bool up_button );
extern void favorite_data_write( uint8_t u8_index );

extern void favorite_data_delete( uint8_t u8_index );

extern uint8_t favorite_name_find( uint8_t *u8_data, uint8_t u8_size );

#endif //_FAVORITE_MANAGER_H_
