#ifndef _UART_EVENT_H_
#define _UART_EVENT_H_

#define UART_TX_BUF_SIZE 256                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 256                         /**< UART RX buffer size. */

extern void uart_close(void);
extern void uart_init(void);
extern void uart_send_data( uint8_t* ui8_byte, uint16_t ui16_length );

extern void uart_rx_queue( void );

extern void uart_tx_reset( void );
extern void uart_tx_queue( void );
extern void uart_tx_push( uint8_t* ui8_byte, uint16_t ui16_length );
#endif
