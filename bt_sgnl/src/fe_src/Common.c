#include "Common.h"

#ifdef ENABLE_FE_TEST
void preemphasis(int len, float *buf, float *prev)
{
    short k;
    float temp;

    for (k = 0; k < len; k++)
    {
        temp = buf[k];
        buf[k] -= (ALPHA **prev);
        *prev = temp;
    }
}

void deemphasis(int len, float *buf, float *prev)
{
    short k;

    for (k = 0; k < len; k++)
    {
        buf[k] += (ALPHA **prev);
        *prev = buf[k];
    }
}
#endif