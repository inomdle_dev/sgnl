#include "global_define.h"

#ifdef ENABLE_BLE_UART

void ble_protocol(uint8_t* u8_data, uint8_t u8_size)
{
	uint8_t u8_data_type = u8_data[1];
	
	g_str_bitflag.b3_ble_report = u8_data_type;
	switch( u8_data_type )
	{
	case BLE_REPORT_BT_PAIRING_ON :
		//bt pairing mode on
		g_str_bitflag.b1_pairing_flag = 1;
#ifdef ENABLE_IS206x
		if( BT_IsAllowedToSendCommand() )
		{		
#ifdef ENABLE_SIMPLE_BT
			g_str_timer.ui16_mfb_cnt = 100;//100 �̸� �ȵ�
			g_str_bitflag.b1_bt_conn = 1;
			g_str_bitflag.b2_bt_on_off = 1;
#else
			if( (BT_SystemStatus != BT_SYSTEM_CONNECTED)
			   && (BT_LinkbackStatus != BT_LINK_CONNECTED) )
			{
				BTAPP_TaskReq( BT_REQ_SYSTEM_ON );
			}
#endif //ENABLE_SIMPLE_BT

			ble_command_send_data(BT_PAIRING_STATUS, PAIRING_START);
		}
#endif //ENABLE_IS206x
		break;
		
	case BLE_REPORT_CALL_STATUS :
		g_str_timer.ui16_bt_power_off_cnt = 0;
		g_str_timer.ui16_sleep_cnt = 0;
		switch(u8_data[2])
		{
		case CALL_INCOMING_CALL :
			g_str_bitflag.b3_ble_call_command = CALL_INCOMING_CALL;
			g_str_bitflag.b2_make_call_status = 2;
#ifdef ENABLE_IS206x
#ifdef ENABLE_SIMPLE_BT
			g_str_timer.ui16_mfb_cnt = 100;//100 �̸� �ȵ�
			g_str_bitflag.b1_bt_conn = 1;
			g_str_bitflag.b2_bt_on_off = 1;
#else
			//if( (BT_SystemStatus != BT_SYSTEM_CONNECTED)
			  // && (BT_LinkbackStatus != BT_LINK_CONNECTED) )
			{
				//BTAPP_TaskReq( BT_REQ_SYSTEM_ON );
			}
#endif //ENABLE_SIMPLE_BT
#endif //ENABLE_IS206x
			break;
			
		case CALL_RECEIVE_CALL :
			g_str_bitflag.b3_ble_call_command = CALL_RECEIVE_CALL;
			g_str_bitflag.b2_make_call_status = 2;
			break;
			
		case CALL_END_CALL :
			g_str_bitflag.b3_ble_call_command = CALL_END_CALL;
#ifdef ENABLE_STANDBY
			g_str_bt.ui16_bt_mode_count = SLEEP_COUNT;
#endif
#ifdef ENABLE_IS206x
			if( BT_IsAllowedToSendCommand() )
			{
				BT_MMI_ActionCommand(FORCE_END_CALL, BT_linkIndex);
			}
#endif //ENABLE_IS206x
			break;
			
		case CALL_MAKE_CALL :
			memset(g_ui8_phone_num, 0x00, sizeof(g_ui8_phone_num));
			g_ui8_phone_num[0] = u8_data[0] - 3;
			strncpy((char *)&g_ui8_phone_num[1], (char *)&u8_data[3], g_ui8_phone_num[0]);

			g_str_bitflag.b3_ble_call_command = CALL_MAKE_CALL;
			g_str_bitflag.b2_make_call_status = 1;
#ifdef ENABLE_IS206x
#ifdef ENABLE_SIMPLE_BT
			if( simple_status == SIMPLE_BT_HFP_CONNECTED )
			{
				BT_MakeCall(g_ui8_phone_num);
			}
			else if( simple_status == SIMPLE_BT_OFF )
			{
				bt_simple_power_on();
			}
#else
			if( (BT_SystemStatus == BT_SYSTEM_CONNECTED)
			   && (BT_LinkbackStatus == BT_LINK_CONNECTED) )
#endif //ENABLE_SIMPLE_BT
			{
				BT_MakeCall(g_ui8_phone_num);
			}
#endif //ENABLE_IS206x
			break;
			
		case CALL_OUTGOING_CALL:
			g_str_bitflag.b3_ble_call_command = CALL_OUTGOING_CALL;
#ifdef ENABLE_STANDBY
			g_str_bt.ui16_bt_mode_count = WAKEUP_SLEEP_COUNT;
#endif
			break;
			
		default : break;
		}  

	case BLE_REPORT_VOLUME_CONTROL:
			switch(u8_data[2])
			{
			case VOLUME_UP:
#ifdef ENABLE_VOL_SHIFT
				if( g_device_status == DEVICE_CALLING )
				{
#ifdef ENABLE_AUDIO
					if( g_str_audio.ui16_vol_level < 5 )
					{
						g_str_audio.ui16_vol_level++;
					}
					else
					{
						g_str_audio.ui16_vol_level = 5;
					}
#endif
				}
#endif
				break;
				
			case VOLUME_DOWN:
#ifdef ENABLE_VOL_SHIFT
				if( g_device_status == DEVICE_CALLING )
				{
#ifdef ENABLE_AUDIO
					if( g_str_audio.ui16_vol_level > 0 )
					{
						g_str_audio.ui16_vol_level--;
					}
					else
					{
						g_str_audio.ui16_vol_level = 0;
					}
#endif
				}
#endif
				break;
				
			default: break;
			}
			break;
		
		case BLE_REPORT_DEFAULT_RESET:
#ifdef ENABLE_SIMPLE_BT
			bt_simple_default_reset();
			g_str_bitflag.b1_pairing_flag = 0;
#else
			BT_MMI_ActionCommand(RESET_EEPROM_SETTING, 0);
#endif
		break;
		
	default : break;
	
	}
}

#endif //ENABLE_BLE_UART
