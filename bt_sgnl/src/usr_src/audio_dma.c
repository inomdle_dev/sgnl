#include "global_define.h"

#ifdef ENABLE_AUDIO_DMA

DMA_InitTypeDef DMA_InitStructure;


void Audio_MAL_IRQHandler(void);

void Audio_DMA_config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the DMA clock */
	RCC_AHB1PeriphClockCmd(AUDIO_I2S_DMA_CLOCK, ENABLE); 
	
	/* Configure the DMA Stream */
	DMA_Cmd(AUDIO_I2S_DMA_STREAM, DISABLE);
	DMA_DeInit(AUDIO_I2S_DMA_STREAM);
	/* Set the parameters to be configured */
	DMA_InitStructure.DMA_Channel = AUDIO_I2S_DMA_CHANNEL;  
	DMA_InitStructure.DMA_PeripheralBaseAddr = AUDIO_I2S_DMA_DREG;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)0;      /* This field will be configured in play function */
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = (uint32_t)AUDIO_FRAME_SIZE*AUDIO_FRAME_BUFFER-1;      /* This field will be configured in play function */
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = AUDIO_I2S_DMA_PERIPH_DATA_SIZE;
	DMA_InitStructure.DMA_MemoryDataSize = AUDIO_I2S_DMA_MEM_DATA_SIZE; 
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;//DMA_Mode_Normal;
	
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;  
	DMA_Init(AUDIO_I2S_DMA_STREAM, &DMA_InitStructure);  
	
#if 0
	/* Enable the selected DMA interrupts (selected in "stm32f4_discovery_eval_audio_codec.h" defines) */
	DMA_ITConfig(AUDIO_I2S_DMA_STREAM, DMA_IT_TC, ENABLE);
	
	/* I2S DMA IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = AUDIO_I2S_DMA_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif
	/* Enable the I2S DMA request */
	SPI_I2S_DMACmd(AUDIO_I2S_TX, SPI_I2S_DMAReq_Tx, ENABLE);  
}

void Audio_MAL_IRQHandler(void)
{    
	
	//#ifdef AUDIO_I2S_DMA_IT_TC_EN
	/* Transfer complete interrupt */
	if (DMA_GetFlagStatus(AUDIO_I2S_DMA_STREAM, AUDIO_I2S_DMA_FLAG_TC) != RESET)
	{         
		DMA_Cmd(AUDIO_I2S_DMA_STREAM, DISABLE);   
		
		/* Clear the Interrupt flag */
		DMA_ClearFlag(AUDIO_I2S_DMA_STREAM, AUDIO_I2S_DMA_FLAG_TC);       
		
		/* Manage the remaining file size and new address offset: This function 
		should be coded by user (its prototype is already declared in stm32f4_discovery_audio_codec.h) */  
	}
	//#endif /* AUDIO_I2S_DMA_IT_TC_EN */
	
}

void Audio_MAL_Play(uint32_t Addr, uint32_t Size)
{         
	/* Configure the buffer address and size */
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)Addr;
	DMA_InitStructure.DMA_BufferSize = (uint32_t)Size;
	
	/* Configure the DMA Stream with the new parameters */
	DMA_Init(AUDIO_I2S_DMA_STREAM, &DMA_InitStructure);
	
	/* Enable the I2S DMA request */
	SPI_I2S_DMACmd(AUDIO_I2S_TX, SPI_I2S_DMAReq_Tx, ENABLE);  
	
	/* Enable the I2S DMA Stream*/
	DMA_Cmd(AUDIO_I2S_DMA_STREAM, ENABLE);   
	
	
	/* If the I2S peripheral is still not enabled, enable it */
	if ((AUDIO_I2S_TX->I2SCFGR & I2S_ENABLE_MASK) == 0)
	{
		I2S_Cmd(AUDIO_I2S_TX, ENABLE);
	}
}
void Audio_MAL_Stop(void)
{   
	/* Stop the Transfer on the I2S side: Stop and disable the DMA stream */
	/* Enable the I2S DMA request */
	SPI_I2S_DMACmd(AUDIO_I2S_TX, SPI_I2S_DMAReq_Tx, DISABLE);  
	DMA_Cmd(AUDIO_I2S_DMA_STREAM, DISABLE);
	
	/* In all modes, disable the I2S peripheral */
	I2S_Cmd(AUDIO_I2S_TX, DISABLE);
}

void Audio_I2S_IRQHandler(void)
{
	/* Check on the I2S TXE flag */  
	if (SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE) != RESET)
	{ 
		/* Send dummy data on I2S to avoid the underrun condition */
		SPI_I2S_SendData(AUDIO_I2S_TX, 0); 
	}
}

#endif
