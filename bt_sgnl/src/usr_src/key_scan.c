#include "global_define.h"
#include "key_event.h"
#include "key_scan.h"

void key_scan( void )
{
	switch(g_key_event)
	{
	case KEY_EVENT_POWER :
		if( g_key_state & KEY_POWER_PRESS )
		{
			g_str_timer.ui16_long_press_cnt++;
		}
		else if( g_key_state & KEY_POWER_RELEASE )
		{
			g_str_timer.ui16_long_press_cnt = 0;
			g_key_state &= ~KEY_POWER_RELEASE;
			g_key_event = KEY_EVENT_NONE;
		}
		break;
		
	case KEY_EVENT_VOLUP :
		if( g_key_state & KEY_VOLUP_PRESS )
		{
			g_str_timer.ui16_long_press_cnt++;
		}
		else if( g_key_state & KEY_VOLUP_RELEASE )
		{
			g_str_timer.ui16_long_press_cnt = 0;
			g_key_state &= ~KEY_VOLUP_RELEASE;
			g_key_event = KEY_EVENT_NONE;
		}
		break;
	case KEY_EVENT_VOLDN :
		if( g_key_state & KEY_VOLDN_PRESS )
		{
			g_str_timer.ui16_long_press_cnt++;
		}
		else if( g_key_state & KEY_VOLDN_RELEASE )
		{
			g_str_timer.ui16_long_press_cnt = 0;
			g_key_state &= ~KEY_VOLDN_RELEASE;
			g_key_event = KEY_EVENT_NONE;
		}
		break;
	default :
		break;
	}
}

void key_action( void )
{
	if( g_str_timer.ui16_long_press_cnt > 100 )
	{
		//long press chk
		if( g_str_timer.ui16_long_press_cnt > 300 )
		{
			key_long_press();
		}
		
		if( g_key_state & KEY_ANY_RELEASE )
		{
			g_key_state = KEY_NONE;
			g_str_timer.ui16_long_press_cnt = 0;
		}
	}
	else if( g_str_timer.ui16_long_press_cnt > 10 )
	{
		if( g_key_state & KEY_ANY_RELEASE )
		{
			key_short_press();
		}
	}
	
}

void key_long_press( void )
{
	switch ( g_key_state )
	{
	case KEY_POWER_PRESS :
		if( g_device_status == DEVICE_INCOMING_CALL )
		{
#ifdef ENABLE_IS206x
			BTAPP_CallEventLong();
#endif
			
#ifdef ENABLE_DATA_LOG
			write_log_flash(LOG_CALL_STATUS_REJECT);
#endif //ENABLE_DATA_LOG
			g_str_timer.ui16_long_press_cnt = 0;
		}
		
		if( g_str_timer.ui16_long_press_cnt > 500 )
		{
			g_str_timer.ui16_long_press_cnt = 0;
			g_key_state &= ~KEY_POWER_RELEASE;
			
#ifdef ENABLE_DATA_LOG
			if( g_str_bitflag.b1_power_down == 0 )
			{
				write_log_flash(LOG_POWER_OFF_USER);
			}
#endif
			g_str_bitflag.b1_power_down = 1;
			
		}
		break;
	case KEY_VOLUP_PRESS :
		//do not disturb off
		if (g_device_status == DEVICE_DO_NOT_DISTURB )
		{
			g_device_status = DEVICE_WAIT_EVENT;
		}
		g_key_state &= ~KEY_VOLUP_RELEASE;

#if 0
		BT_MMI_ActionCommand(RESET_EEPROM_SETTING, 0);
		BT_MMI_ActionCommand(ANY_MODE_ENTERING_PAIRING, 0);
#else
		BT_MMI_ActionCommand(RESET_EEPROM_SETTING, 0);
		BTAPP_EnterBTPairingMode();
#endif
		//BT_LinkBackToLastDevice();
		break;
	case KEY_VOLDN_PRESS :
		//do not disturb on
		if (g_device_status == DEVICE_WAIT_EVENT )
		{
			g_device_status = DEVICE_DO_NOT_DISTURB;
		}
		
		g_key_state &= ~KEY_VOLDN_RELEASE;
		break;
	case KEY_PWRUP_PRESS :
#ifdef ENABLE_IS206x
		{
			BTAPP_Init();
			while(1)
			{
				BTAPP_Task();
			
				if( BTAPP_GetStatus() == BT_STATUS_OFF )
				{
					BTAPP_TaskReq( BT_REQ_SYSTEM_ON );
					break;
				}
			}
		}
		BTAPP_ResetEEPROMtoDefault();
		BTAPP_EnterBTPairingMode();
		
#ifdef ENABLE_DATA_LOG
		reset_log_flash();
		write_log_flash(LOG_ENTER_PAIRING);
#endif //ENABLE_DATA_LOG
		
#endif
		break;
	case KEY_UPDN_PRESS :
		break;
	case KEY_PWRUPDN_PRESS :
		break;
		
	default :
		break;
	}
	
	if( !( g_key_state & KEY_POWER_PRESS ) )
	{
		g_str_timer.ui16_long_press_cnt = 0;
		g_key_state = KEY_NONE;
	}
}

void key_short_press( void )
{
	switch ( g_key_state )
	{
	case KEY_POWER_SHORT :
	case KEY_POWER_RELEASE :
		//g_key_state &= ~KEY_POWER_SHORT;
#ifdef ENABLE_FAVORITE_CALL
		if( g_device_status == DEVICE_FAVORITE_CALL )
		{
			make_favorite_call(g_ui16_favorite_index);

			g_str_timer.ui16_favorite_cnt = 0;
			
		}
		else
#endif //ENABLE_FAVORITE_CALL
			
		{
#ifdef ENABLE_IS206x
			BTAPP_CallEventShort();
#endif
		}
		
		break;
	case KEY_VOLUP_SHORT :
	case KEY_VOLUP_RELEASE :
		{
#ifdef ENABLE_FAVORITE_CALL
			if( g_device_status == DEVICE_WAIT_EVENT )
			{
				g_device_status = DEVICE_FAVORITE_CALL;
				
			}
			
			else if( g_device_status == DEVICE_FAVORITE_CALL )
			{
				if( g_ui16_favorite_index < 4 )
				{
					g_ui16_favorite_index++;
				}
				else
				{
					g_ui16_favorite_index = 0;
				}
				
				
				g_str_timer.ui16_favorite_cnt = 0;
			}
#endif //ENABLE_FAVORITE_CALL
			
#ifdef ENABLE_VOL_SHIFT
			else if( g_device_status == DEVICE_CALLING )
			{
				if( g_str_audio.ui16_vol_level < 5 )
				{
					g_str_audio.ui16_vol_level++;
				}
				else
				{
					g_str_audio.ui16_vol_level = 5;
				}
			}
#endif
			//g_key_state &= ~KEY_VOLUP_SHORT;
		}
		break;
	case KEY_VOLDN_SHORT :
	case KEY_VOLDN_RELEASE :
		
		{
#ifdef ENABLE_FAVORITE_CALL
			if( g_device_status == DEVICE_WAIT_EVENT )
			{
				g_device_status = DEVICE_FAVORITE_CALL;
				
			}
			else if( g_device_status == DEVICE_FAVORITE_CALL )
			{
				if( g_ui16_favorite_index > 0 )
				{
					g_ui16_favorite_index--;
				}
				else
				{
					g_ui16_favorite_index = 4;
				}
				
				
				g_str_timer.ui16_favorite_cnt = 0;
			}
#endif //ENABLE_FAVORITE_CALL
			
#ifdef ENABLE_VOL_SHIFT
			else if( g_device_status == DEVICE_CALLING )
			{
				if( g_str_audio.ui16_vol_level > 0 )
				{
					g_str_audio.ui16_vol_level--;
				}
				else
				{
					g_str_audio.ui16_vol_level = 0;
				}
			}
#endif

#if 0
//pedometer flash test
			g_ui32_pedometer_call_app = 1;
			if(g_ui32_app_date >= 161205)
			{
				g_ui32_mcu_hour = 1;
			}
			else
			{
				//flash_erase(FLASH_ADDR_START_PEDO,FLASH_ADDR_END_PEDO);
				g_ui32_mcu_hour = 0;
				//g_ui32_app_date = 170217;
				g_ui32_app_date = 161205;
				g_ui16_app_hour = 12;
			}
#endif
			//g_key_state &= ~KEY_VOLDN_SHORT;
			break;
		}
	default :
		break;
	}
	
	g_str_timer.ui16_long_press_cnt = 0;
	g_key_state = KEY_NONE;
}