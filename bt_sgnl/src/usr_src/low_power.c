#include "global_define.h"

#ifdef ENABLE_STANDBY

void enter_standby_mode(void)
{
	if(BTAPP_GetStatus() == BT_STATUS_OFF)
	{
		//power off sequence
		if( g_str_timer.ui16_bt_power_off_cnt == 100 )
		{
			BT_MFB_DISABLE;
		}
		else if( g_str_timer.ui16_bt_power_off_cnt == 200 )
		{
			BT_NRST_DISABLE;
		}
		else if( g_str_timer.ui16_bt_power_off_cnt == 300 )
		{
#ifdef ENABLE_BLE_UART
			USART_ITConfig(BLE_UART, USART_IT_RXNE, DISABLE);
#endif
#ifdef ENABLE_BT_UART
			USART_ITConfig(BT_UART, USART_IT_RXNE, DISABLE);
#endif
#ifdef ENABLE_IS206x
#ifdef ENABLE_SIMPLE_BT
			if( (simple_status == SIMPLE_BT_OFF) && (!g_str_timer.ui16_mfb_cnt) )
#else
			//if(BTAPP_GetStatus() == BT_STATUS_OFF)
#endif
#endif
			{
				g_str_bitflag.b1_power_down = 0;
				g_str_timer.ui16_bt_power_off_cnt = 0;
				
				PWR_EnterSTANDBYMode();
			}
		}
	}
}

#ifdef ENABLE_STANDBY
void wakeup_isr(void)
{
	if (EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
		SystemInit();
		
#ifdef ENABLE_SIMPLE_BT
		if( ( simple_status == SIMPLE_BT_OFF ) || ( BT_CallStatus == BT_CALL_IDLE ) )
#else
		if( BTAPP_GetStatus() == BT_STATUS_OFF )
#endif
		{
			struct_init();

#ifdef ENABLE_IS206x
			BTAPP_Init();

			BT_MFB_DISABLE;
#ifdef ENABLE_SIMPLE_BT
			BT_NRST_ENABLE;
#else
			BT_NRST_DISABLE;
#endif
			
#ifdef ENABLE_BLE_UART
			USART_ITConfig(BLE_UART, USART_IT_RXNE, ENABLE);
			ble_command_send_data(MCU_STATUS, MCU_READY);
#endif
#ifdef ENABLE_BT_UART
			USART_ITConfig(BT_UART, USART_IT_RXNE, ENABLE);
#endif
			
#endif
		}

#ifdef ENABLE_IS206x
#ifdef ENABLE_SIMPLE_BT
		g_str_timer.ui16_mfb_cnt = 150;//90 �̸� �ȵ�
		g_str_bitflag.b1_bt_conn = 1;
		g_str_bitflag.b2_bt_on_off = 1;
#else
		BTAPP_TaskReq( BT_REQ_SYSTEM_ON );
#endif //ENABLE_SIMPLE_BT
#endif //ENABLE_IS206x
		g_str_timer.ui16_sleep_cnt = 0;
		g_str_timer.ui16_bt_power_off_cnt = 0;
		g_str_bitflag.b1_power_down = 0;
		
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}
#endif

void set_sleepMode( void )
{
#if 01

#ifdef ENABLE_IS206x
#ifdef ENABLE_SIMPLE_BT
	if(simple_status != SIMPLE_BT_OFF)
		bt_simple_power_off();
#else
	BT_MMI_ActionCommand(DISCONNECT_HF_LINK, BT_linkIndex);
	BTAPP_TaskReq( BT_REQ_SYSTEM_OFF );
#endif
	
#endif

#ifdef ENABLE_AUDIO
	AMP_STATUS_DISABLE;
	I2S_Cmd(AUDIO_I2S_TX, DISABLE);
	I2S_Cmd(AUDIO_I2S_RX, DISABLE);
#endif	
	g_str_bitflag.b1_power_down = 1;
	g_str_timer.ui16_bt_power_off_cnt = 0;
	g_str_timer.ui16_sleep_cnt = 0;

	ble_command_send_data(MCU_STATUS, MCU_SLEEP);
#endif
}

#endif //ENABLE_STANDBY
