#include "global_define.h"

uint8_t g_ui8_phone_num[20] = {0,};
bitflag_struct_t g_str_bitflag;

device_status_t g_device_status;
BT_LinkbackStatus_e BT_LinkbackStatus;
BT_SystemStatus_e BT_SystemStatus;
BT_CallStatus_e BT_CallStatus;

BT_CMD_SEND_STATE BT_CMD_SendState;
uint8_t BT_CommandStartMFBWaitTimer;
uint8_t BT_CommandSentMFBWaitTimer;

#ifdef ENABLE_BT_UART
bt_uart_struct_t g_str_bt;
#endif

#ifdef ENABLE_BLE_UART
bt_uart_struct_t g_str_ble;
#endif //ENABLE_BLE_UART

#ifdef ENABLE_AUDIO
audio_struct_t g_str_audio;
#endif

timer_struct_t g_str_timer;
uint16_t g_ui16_dbg_cnt = 0;

#ifdef ENABLE_FE
fe_struct_t g_str_fe;
#endif

#ifdef ENABLE_OTA
ota_struct_t g_str_ota;
#endif

#ifdef ENABLE_AUDIO
uint16_t g_ui16_rx_idx = 0;
uint16_t g_ui16_tx_idx = 0;
#endif


void idletest( void )
{
	uint32_t jump_addr;
	
	jump_addr = *(__IO uint32_t*) (FLASH_ADDR_JUMP_APP);
	if(jump_addr == FLASH_ADDR_CURRENT_APP)
		return;// FLASH_ERROR_NONE;
	
	/* Unlock the Flash to enable the flash control register access *************/ 
	FLASH_Unlock();
	
	/* Erase the user Flash area
	(area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/
	
	/* Clear pending flags (if any) */  
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
			FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR); 
	
	/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
	be done by word */
	if(FLASH_EraseSector(ERASE_LOG_START_SECTOR, VoltageRange_3) != FLASH_COMPLETE)
	{ 
		/* Error occurred while sector erase. 
		User can add here some code to deal with this error  */
		//ret = FLASH_ERROR_ERASE;
		FLASH_Lock();
		return;
	}
	
	if(FLASH_ProgramWord(FLASH_ADDR_JUMP_APP, FLASH_ADDR_CURRENT_APP)
	   != FLASH_COMPLETE)
	{
		//ret = FLASH_ERROR_WRITE;
	}
	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) *********/
	FLASH_Lock();
}

void peripheral_init(void)
{
	pin_enable_init();
#ifdef ENABLE_MONITOR
	monitor_init();
#endif
	
#ifdef ENABLE_BT_UART
	bt_uart_init();
#endif
	
#ifdef ENABLE_BLE_UART
	ble_uart_init();
#endif
	
#ifdef ENABLE_AUDIO
	audio_init(AUDIO_INPUT_8K);
#endif
	
#ifdef ENABLE_IS206x
	BTAPP_Init();
	BT_CallStatus = BT_CALL_IDLE;
#endif
}

void struct_init(void)
{
	memset(&g_str_bitflag, 0, sizeof(bitflag_struct_t));
	
	g_device_status = DEVICE_STANDBY;
	
#ifdef ENABLE_BT_UART
	memset(&g_str_bt, 0, sizeof(bt_uart_struct_t));
	g_str_bt.ui16_tx_cnt = BT_UART_BUFFERSIZE;
#ifdef ENABLE_STANDBY
	g_str_bt.ui16_bt_mode_count = SLEEP_COUNT;
#endif
#endif //ENABLE_BT_UART
	
#ifdef ENABLE_BLE_UART
	memset(&g_str_ble, 0, sizeof(bt_uart_struct_t));
	g_str_ble.ui16_tx_cnt = BLE_UART_BUFFERSIZE;
#endif	
	
#ifdef ENABLE_AUDIO
	memset(&g_str_audio, 0, sizeof(audio_struct_t));
	
#ifdef ENABLE_VOL_SHIFT
	g_str_audio.ui16_vol_level = 3;
#endif
#ifdef ENABLE_FE
	memset(&g_str_fe, 0, sizeof(fe_struct_t));
#endif
#endif
	
	memset(&g_str_timer, 0, sizeof(timer_struct_t));
	
#ifdef ENABLE_OTA
	g_str_ota.ui16_state = OTA_STATE_NONE;
#endif //ENABLE_OTA
	
#ifdef ENABLE_FREQ_CHECK
	g_str_bitflag.b3_freq_chk = FREQ_CHK_READY;
#endif
}

uint16_t g_ui16_loop_cnt = 0;
int main( void )
{
#if 01
	//peripheral configuration initialize
	peripheral_init(); 
	
	PWR_HOLD_ENABLE;
	
	//structure variable initailize
	struct_init();

	while (1)
	{
#ifdef ENABLE_IS206x
	#ifdef ENABLE_SIMPLE_BT
		bt_simple_task();
		if( !g_str_timer.ui16_mfb_cnt )
		{
			if( g_str_bitflag.b2_bt_on_off )// ready to power on
			{
				bt_simple_power_on();
				g_str_timer.ui16_ble_delay_cnt = 0;
				g_str_bitflag.b2_bt_on_off = 0;
			}
			else if( g_str_bitflag.b1_pairing_flag )// ready to pair
			{
				bt_simple_pairing_on();
				g_str_timer.ui16_ble_delay_cnt = 0;
				g_str_bitflag.b1_pairing_flag = 0;
			}
		}
	#else
		BTAPP_Task();
	#endif
#endif
		
#ifdef ENABLE_BLE_UART
		BLE_CommandHandler();

		if(g_str_bitflag.b3_ble_report != BLE_REPORT_INIT)
		{
			if( g_str_bitflag.b3_bt_status == PAIRING_TIMEOUT ) // when pairing count is exceeded
			{
				ble_command_send_data(BT_PAIRING_STATUS, PAIRING_TIMEOUT);
				g_str_bitflag.b3_ble_report = BLE_REPORT_INIT;
				BT_MMI_ActionCommand(EXIT_PAIRING_MODE, 0);
				g_str_timer.ui16_bt_power_off_cnt = 0;
				g_str_timer.ui16_sleep_cnt = 0;
				g_str_bitflag.b1_power_down = 0;
			}
			else if( g_str_timer.ui16_ble_delay_cnt >= 50 ) //100
			{
				if ( BT_IsAllowedToSendCommand() )
				{
					if( g_str_bitflag.b3_ble_call_command == CALL_MAKE_CALL )
					{
						BT_MakeCall(g_ui8_phone_num);
						g_str_timer.ui16_ble_delay_cnt = 0;
						g_str_bitflag.b2_make_call_status = 0;
#ifdef ENABLE_AUDIO
						SPI_I2S_ITConfig(AUDIO_I2S_TX, SPI_I2S_IT_TXE, ENABLE);
						SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_RXNE, ENABLE);
						SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE, ENABLE);
						audio_rx_start();
						AMP_STATUS_ENABLE;
#endif
					}
					else if( g_str_bitflag.b3_ble_call_command == CALL_RECEIVE_CALL )
					{
						BT_MMI_ActionCommand(ACCEPT_CALL, 0);
						g_str_timer.ui16_ble_delay_cnt = 0;
						g_str_bitflag.b2_make_call_status = 0;
#ifdef ENABLE_AUDIO
						SPI_I2S_ITConfig(AUDIO_I2S_TX, SPI_I2S_IT_TXE, ENABLE);
						SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_RXNE, ENABLE);
						SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE, ENABLE);
						audio_rx_start();
						AMP_STATUS_ENABLE;
#endif
					}
					else if( g_str_bitflag.b3_ble_call_command == CALL_OUTGOING_CALL )
					{
						g_str_timer.ui16_sleep_cnt = 0;
					}
					else if( g_str_bitflag.b1_pairing_flag == 1 ) //(g_str_bitflag.b3_ble_report == BLE_REPORT_BT_PAIRING_ON)
					{
						BT_MMI_ActionCommand(RESET_EEPROM_SETTING, 0);
						BT_MMI_ActionCommand(ANY_MODE_ENTERING_PAIRING, 0);
						g_str_timer.ui16_ble_delay_cnt = 0;
						g_str_bitflag.b1_pairing_flag = 0;
					}
				}
			}
		}
#endif
		
#ifdef ENABLE_STANDBY
		if( !g_str_bitflag.b1_power_down ) // mcu is working
		{
			if(g_str_timer.ui16_sleep_cnt >= g_str_bt.ui16_bt_mode_count)
			{
				set_sleepMode();
			}
		}
		else // standby mode
		{
			enter_standby_mode();
		}
#endif

#ifdef ENABLE_AUDIO
#ifdef ENABLE_FREQ_CHECK
		audio_freq_check();
#endif		
		if( g_str_bitflag.b1_frame_state == AUDIO_FRAME_ERR )
		{
			audio_rx_start();
		}
#endif
	}
#else
	pin_enable_init();

	AMP_STATUS_DISABLE;
	
	ble_uart_init();
	fe_test_init();
#endif
	
	return 0;
}

void HardFault_Handler(void)
{
	SCB->AIRCR = (0x05FA0000) | 0x04;
}
