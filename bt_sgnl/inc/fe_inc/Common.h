#define ALPHA 0.8f
#define PI2  6.283185307f
#define PI1  3.141592653f

//#define AMR_NB
//#define FIXED
//#define UART_OUTPUT

#define Hz 62.5F /* 8000/128 */

#define NO_ERROR		0

#define M			10
#define FFTSIZE			128
#define FFTSIZE_BY_TWO		64
#define FRAMESIZE		80
#define Fs			8000
#define FRONTMARGIN		48

extern int FrameNo;

void r_fft(float *farray_ptr, int isign);
void preemphasis(int len,float *buf,float *prev);
void deemphasis(int len,float *buf,float *prev);
