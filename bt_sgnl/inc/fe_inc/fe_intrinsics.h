#ifndef INTRINSICS_H

#define INTRINSICS_H

int _iabs(int val);
int _iround(int Val, int Bit);


#define iabs(val)               _iabs(val)
#define iround(Val, Bit)         _iround(Val, Bit)
#define smul32x32(a, b, c)      (int)(((long long)(a) * (long long)(b)) >> (c))
#define imin(a,b)		        ((a)<(b)?(a):(b))
#define imax(a,b)		        ((a)>(b)?(a):(b))
#define isquare(a)		        ((a)*(a))


#endif
