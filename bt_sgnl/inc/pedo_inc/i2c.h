/*********************************************************************
File    : i2c.h
Purpose : 
**********************************************************************/
#ifndef __I2C_H__
#define __I2C_H__
/****************************** Includes *****************************/
#include "tm_stm32f4_i2c.h"
#include "tm_stm32f4_gpio.h"

/****************************** Defines *******************************/
#define I2C_SPEED                 400000
#define I2C_OWN_ADDRESS           0x00

#define DAC_I2C				I2C1
#define DAC_I2C_SCL_GPIO_PORT		GPIOB
#define DAC_I2C_SCL_GPIO_CLK		RCC_AHB1Periph_GPIOB
#define DAC_I2C_SCL_GPIO_PIN		GPIO_Pin_6
#define DAC_I2C_SCL_GPIO_PINSOURCE	GPIO_PinSource6

#define DAC_I2C_SDA_GPIO_PORT		GPIOB
#define DAC_I2C_SDA_GPIO_CLK		RCC_AHB1Periph_GPIOB
#define DAC_I2C_SDA_GPIO_PIN		GPIO_Pin_7
#define DAC_I2C_SDA_GPIO_PINSOURCE	GPIO_PinSource7

#define DAC_I2C_RCC_CLK			RCC_APB1Periph_I2C1
#define DAC_I2C_AF			GPIO_AF_I2C1

#define PEDO_I2C			I2C3
#define PEDO_I2C_SCL_GPIO_PORT		GPIOA
#define PEDO_I2C_SCL_GPIO_CLK		RCC_AHB1Periph_GPIOA
#define PEDO_I2C_SCL_GPIO_PIN		GPIO_Pin_8
#define PEDO_I2C_SCL_GPIO_PINSOURCE	GPIO_PinSource8

#define PEDO_I2C_SDA_GPIO_PORT		GPIOB
#define PEDO_I2C_SDA_GPIO_CLK		RCC_AHB1Periph_GPIOB
#define PEDO_I2C_SDA_GPIO_PIN		GPIO_Pin_8
#define PEDO_I2C_SDA_GPIO_PINSOURCE	GPIO_PinSource8

#define PEDO_I2C_RCC_CLK		RCC_APB1Periph_I2C3
#define PEDO_I2C_AF			GPIO_AF_I2C3

void i2c_init(void);

int Sensors_I2C_ReadRegister(I2C_TypeDef* SENSORS_I2C, unsigned char Address, unsigned char RegisterAddr, unsigned short RegisterLen, unsigned char *RegisterValue);
int Sensors_I2C_WriteRegister(I2C_TypeDef* SENSORS_I2C, unsigned char Address, unsigned char RegisterAddr, unsigned short RegisterLen, const unsigned char *RegisterValue);

unsigned long ST_Sensors_I2C_WriteRegister(I2C_TypeDef* SENSORS_I2C, unsigned char Address, unsigned char RegisterAddr, unsigned short RegisterLen, const unsigned char *RegisterValue);
unsigned long ST_Sensors_I2C_ReadRegister(I2C_TypeDef* SENSORS_I2C, unsigned char Address, unsigned char RegisterAddr, unsigned short RegisterLen, unsigned char *RegisterValue);

#endif // __I2C_H__


