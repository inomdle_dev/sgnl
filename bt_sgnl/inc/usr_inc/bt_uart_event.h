#ifndef _BT_UART_EVENT_H_
#define _BT_UART_EVENT_H_

/* USART peripheral configuration defines */
#define BT_UART					USART1
#define BT_UART_CLK				RCC_APB2Periph_USART1
#define BT_UART_IRQn				USART1_IRQn

#define BT_UART_GPIO_PORT			GPIOA
#define BT_UART_GPIO_CLK			RCC_AHB1Periph_GPIOA
#define BT_UART_AF				GPIO_AF_USART1

#define BT_UART_TX_PIN				GPIO_Pin_9
#define BT_UART_TX_SOURCE			GPIO_PinSource9

#define BT_UART_RX_PIN				GPIO_Pin_10
#define BT_UART_RX_SOURCE			GPIO_PinSource10

#define BT_UART_BAUDRATE				115200
#define BT_UART_BUFFERSIZE				256

void bt_uart_init( void );
void bt_uart_config( void );
void bt_uart_send_byte(uint8_t ui8_byte);
uint8_t bt_uart_read_byte( void );

extern uint8_t device_status;

#define bt_uart_isr					USART1_IRQHandler

#endif //_BT_UART_EVENT_H_
