#ifndef DBG_CODE_H
#define DBG_CODE_H

/* USART peripheral configuration defines */
#define DBG_UART				USART6
#define DBG_UART_CLK				RCC_APB2Periph_USART6
#define DBG_UART_IRQn				USART6_IRQn

#define DBG_UART_GPIO_PORT			GPIOA
#define DBG_UART_GPIO_CLK			RCC_AHB1Periph_GPIOA
#define DBG_UART_AF				GPIO_AF_USART6

#define DBG_UART_TX_PIN				GPIO_Pin_12
#define DBG_UART_TX_SOURCE			GPIO_PinSource12

#define DBG_UART_RX_PIN				GPIO_Pin_11
#define DBG_UART_RX_SOURCE			GPIO_PinSource11

#define DBG_UART_BAUDRATE			115200
#define DBG_UART_BUFFERSIZE			256

void dbg_init( void );
void dbg_uart_config( void );
void dbg_send_byte(uint8_t ui8_byte);
uint8_t dbg_read_byte( void );
void dbg_parser( void );
#if 0
void dbg_print( uint8_t byte[] );
#else
void dbg_print( const char *form, ... );
#endif

#define dbg_uart_isr                       USART6_IRQHandler

#endif //DBG_CODE_H