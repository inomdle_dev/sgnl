#ifndef _BLE_UART_EVENT_H_
#define _BLE_UART_EVENT_H_

/* USART peripheral configuration defines */
#define BLE_UART				USART2
#define BLE_UART_CLK				RCC_APB1Periph_USART2
#define BLE_UART_IRQn				USART2_IRQn

#define BLE_UART_GPIO_PORT			GPIOA
#define BLE_UART_GPIO_CLK			RCC_AHB1Periph_GPIOA
#define BLE_UART_AF				GPIO_AF_USART2

#define BLE_UART_TX_PIN				GPIO_Pin_2
#define BLE_UART_TX_SOURCE			GPIO_PinSource2

#define BLE_UART_RX_PIN				GPIO_Pin_3
#define BLE_UART_RX_SOURCE			GPIO_PinSource3

#define BLE_UART_CTS_PIN			GPIO_Pin_7
#define BLE_UART_CTS_SOURCE			GPIO_PinSource7

#define BLE_UART_RTS_PIN			GPIO_Pin_8
#define BLE_UART_RTS_SOURCE			GPIO_PinSource8

#define ble_uart_isr				USART2_IRQHandler


#define BLE_UART_BAUDRATE			115200

#ifdef ENABLE_BT_UART
#ifndef BLE_UART_BUFFERSIZE
#define BLE_UART_BUFFERSIZE			256
#endif
#endif

void ble_uart_init( void );
void ble_uart_config( void );
uint8_t ble_uart_read_byte( void );

void BLE_CommandHandler( void );

void ble_uart_send_data( uint8_t byte );
void ble_uart_send( uint8_t* ui8_byte, uint16_t ui16_length );
void ble_command_send_data( uint8_t command, uint8_t param );
void ble_lowpower_uart_tx( void );
void ble_lowpower_uart_send( uint8_t* ui8_byte, uint16_t ui16_length );

#endif //_BLE_UART_EVENT_H_
