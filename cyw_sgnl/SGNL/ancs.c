#include "sparcommon.h"
#include "wiced_bt_dev.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_gatt_util.h"
#include "wiced_bt_cfg.h"
#include "wiced_bt_uuid.h"
#include "wiced_bt_app_common.h"
#include "wiced_bt_app_hal_common.h"
#include "wiced_bt_trace.h"
#include "wiced_hal_nvram.h"
#include "wiced_result.h"
#include "hci_control_api.h"
#include "wiced_hal_platform.h"
#include "SGNL_db.h"
#include "wiced_app_cfg.h"
#include "ancs.h"

wiced_bt_ancs_reg_t ancs_client_reg =
{
    .p_discovery_complete_callback = ancs_discovery_complete_callback,
    .p_start_complete_callback	   = ancs_start_complete_callback,
    .p_stop_complete_callback      = ancs_stop_complete_callback,
    .p_notification_callback       = ancs_notification_callback,
};


/*
 * This function will be called when a connection is established
 */
void ancs_connection_up(wiced_bt_gatt_connection_status_t *p_conn_status)
{
    wiced_bt_gatt_status_t  status;

    WICED_BT_TRACE("%s\n", __FUNCTION__);

    ancs_app_state.conn_id = p_conn_status->conn_id;

    // save address of the connected device.
    memcpy(ancs_app_state.remote_addr, p_conn_status->bd_addr, sizeof(ancs_app_state.remote_addr));
    ancs_app_state.addr_type = p_conn_status->addr_type;

    // need to notify ANCS library that the connection is up
    wiced_bt_ancs_client_connection_up(p_conn_status);

    /* Initialize WICED BT ACSS library Start discovery */
    ancs_app_state.discovery_state = ANCS_DISCOVERY_STATE_SERVICE;
    ancs_app_state.started = WICED_FALSE;
    ancs_app_state.ancs_s_handle = 0;
    ancs_app_state.ancs_e_handle = 0;

    wiced_bt_start_advertisements(BTM_BLE_ADVERT_OFF, 0, NULL);

   // perform primary service search
    status = wiced_bt_util_send_gatt_discover(ancs_app_state.conn_id, GATT_DISCOVER_SERVICES_ALL, UUID_ATTRIBUTE_PRIMARY_SERVICE, 1, 0xffff);
    WICED_BT_TRACE("start discover status:%d\n", status);
}

/*
 * This function will be called when connection goes down
 */
void ancs_connection_down(wiced_bt_gatt_connection_status_t *p_conn_status)
{
    WICED_BT_TRACE("%s\n", __FUNCTION__);

    ancs_app_state.conn_id         = 0;
    ancs_app_state.discovery_state = ANCS_DISCOVERY_STATE_SERVICE;
    ancs_app_state.ancs_s_handle    = 0;
    ancs_app_state.ancs_e_handle    = 0;

    memset(&ancs_hostinfo, 0, sizeof(ancs_hostinfo));

    // tell library that connection is down
    wiced_bt_ancs_client_connection_down(p_conn_status);

    // restart advertisements so that iOS can reconnect at will
    wiced_bt_start_advertisements(BTM_BLE_ADVERT_UNDIRECTED_LOW, 0, NULL);
}

/*
 * Process pairing complete event from the stack
 */
void ancs_process_pairing_complete(wiced_bt_dev_pairing_cplt_t *p_pairing_complete)
{
    if (p_pairing_complete->pairing_complete_info.ble.reason == 0)
    {
        // if we started bonding because we could not start client, do it now
        if (!ancs_app_state.started && (ancs_app_state.ancs_s_handle != 0) && (ancs_app_state.ancs_e_handle != 0))
        {
            wiced_bt_ancs_client_start(ancs_app_state.conn_id);
        }
    }
}


/*
 * Process discovery results from the stack
 */
wiced_bt_gatt_status_t ancs_gatt_discovery_result(wiced_bt_gatt_discovery_result_t *p_data)
{
    WICED_BT_TRACE("[%s] conn %d type %d state 0x%02x\n", __FUNCTION__, p_data->conn_id, p_data->discovery_type, ancs_app_state.discovery_state);

    switch (ancs_app_state.discovery_state)
    {
    case ANCS_DISCOVERY_STATE_ANCS:
        wiced_bt_ancs_client_discovery_result(p_data);
        break;

    default:
        if (p_data->discovery_type  == GATT_DISCOVER_SERVICES_ALL)
        {
            if (p_data->discovery_data.group_value.service_type.len == 16)
            {
                WICED_BT_TRACE("%04x e:%04x uuid\n", p_data->discovery_data.group_value.s_handle, p_data->discovery_data.group_value.e_handle);
                if (memcmp(p_data->discovery_data.group_value.service_type.uu.uuid128, ANCS_SERVICE, 16) == 0)
                {
                    WICED_BT_TRACE("ANCS Service found s:%04x e:%04x\n",
                            p_data->discovery_data.group_value.s_handle,
                            p_data->discovery_data.group_value.e_handle);
                    ancs_app_state.ancs_s_handle = p_data->discovery_data.group_value.s_handle;
                    ancs_app_state.ancs_e_handle = p_data->discovery_data.group_value.e_handle;
                }
            }
        }
        else
        {
            WICED_BT_TRACE("!!!! invalid op:%d\n", p_data->discovery_type);
        }
    }
    return WICED_BT_GATT_SUCCESS;
}

/*
 * Process discovery complete from the stack
 */
wiced_bt_gatt_status_t ancs_gatt_discovery_complete(wiced_bt_gatt_discovery_complete_t *p_data)
{
    wiced_result_t result;

    WICED_BT_TRACE("[%s] conn %d type %d state %d\n", __FUNCTION__, p_data->conn_id, p_data->disc_type, ancs_app_state.discovery_state);

    switch (ancs_app_state.discovery_state)
    {
    case ANCS_DISCOVERY_STATE_ANCS:
        wiced_bt_ancs_client_discovery_complete(p_data);
        break;

    default:
        if (p_data->disc_type == GATT_DISCOVER_SERVICES_ALL)
        {
            WICED_BT_TRACE("ANCS:%04x-%04x\n", ancs_app_state.ancs_s_handle, ancs_app_state.ancs_e_handle);

            /* If ancs Service found tell WICED BT ancs library to start its discovery */
            if ((ancs_app_state.ancs_s_handle != 0) && (ancs_app_state.ancs_e_handle != 0))
            {
                ancs_app_state.discovery_state = ANCS_DISCOVERY_STATE_ANCS;
                if (wiced_bt_ancs_client_discover(ancs_app_state.conn_id, ancs_app_state.ancs_s_handle, ancs_app_state.ancs_e_handle))
                    break;
            }
        }
        else
        {
            WICED_BT_TRACE("!!!! invalid op:%d\n", p_data->disc_type);
        }
    }
    return WICED_BT_GATT_SUCCESS;
}


/*
 * The library calls this function when it finishes receiving complete ANCS event
 */
void ancs_notification_callback(uint16_t conn_id, ancs_event_t *p_ancs_event)
{
#ifdef TEST_HCI_CONTROL
    // Allocating a buffer to send the trace
    uint8_t  *p_tx_buf = (uint8_t*)wiced_bt_get_buffer(sizeof (ancs_event_t));

    if (p_tx_buf)
    {
        int len;
        p_tx_buf[0] = p_ancs_event->notification_uid & 0xff;
        p_tx_buf[1] = (p_ancs_event->notification_uid >> 8) & 0xff;
        p_tx_buf[2] = (p_ancs_event->notification_uid >> 16) & 0xff;
        p_tx_buf[3] = (p_ancs_event->notification_uid >> 24) & 0xff;
        p_tx_buf[4] = p_ancs_event->command;
        p_tx_buf[5] = p_ancs_event->category;
        p_tx_buf[6] = p_ancs_event->flags;
        len = 7;
        utl_strcpy(&p_tx_buf[len], p_ancs_event->title);
        len += strlen(p_ancs_event->title);
        p_tx_buf[len++] = 0;
        utl_strcpy(&p_tx_buf[len], p_ancs_event->message);
        len += strlen(p_ancs_event->message);
        p_tx_buf[len++] = 0;
        utl_strcpy(&p_tx_buf[len], p_ancs_event->positive_action_label);
        len += strlen(p_ancs_event->positive_action_label);
        p_tx_buf[len++] = 0;
        utl_strcpy(&p_tx_buf[len], p_ancs_event->negative_action_label);
        len += strlen(p_ancs_event->negative_action_label);
        p_tx_buf[len++] = 0;
        wiced_transport_send_data(HCI_CONTROL_ANCS_EVENT_NOTIFICATION, p_tx_buf, len);
        wiced_bt_free_buffer(p_tx_buf);
    }
#endif

    WICED_BT_TRACE("ANCS notification UID:%d command:%d category:%d flags:%04x\n", p_ancs_event->notification_uid, p_ancs_event->command, p_ancs_event->category, p_ancs_event->flags);
    WICED_BT_TRACE("title:'%s' message:'%s' positive:'%s' negative:'%s'\n", p_ancs_event->title, p_ancs_event->message, p_ancs_event->positive_action_label, p_ancs_event->negative_action_label);

    wiced_bt_free_buffer(p_ancs_event);
}

/*
 * ANCS server discovery complete
 */
void ancs_discovery_complete_callback(uint16_t conn_id, wiced_bool_t result)
{
    WICED_BT_TRACE("[%s] result:%d\n", __FUNCTION__, result);

    // This app automatically starts the client
    wiced_bt_ancs_client_start(ancs_app_state.conn_id);
}

/*
 * ANCS server start complete
 */
void ancs_start_complete_callback(uint16_t conn_id, wiced_bt_gatt_status_t result)
{
    wiced_result_t rc;

    WICED_BT_TRACE("[%s] result:%d\n", __FUNCTION__, result);
#ifdef TEST_HCI_CONTROL
    wiced_transport_send_data(HCI_CONTROL_ANCS_EVENT_CONNECTED, &result, 1);
#endif
    // Special case when we try to register for notification and we are not paired yet
    if (result == WICED_BT_GATT_INSUF_AUTHENTICATION)
    {
        rc = wiced_bt_dev_sec_bond(ancs_app_state.remote_addr, ancs_app_state.addr_type, BT_TRANSPORT_LE, 0, NULL);
        WICED_BT_TRACE("start bond result:%d\n", rc);
        return;
    }
    if (result == WICED_BT_GATT_SUCCESS)
    {
        ancs_app_state.started = WICED_TRUE;
    }
}

/*
 * ANCS server stop complete
 */
void ancs_stop_complete_callback(uint16_t conn_id, wiced_bt_gatt_status_t result)
{
    WICED_BT_TRACE("[%s] result:%d\n", __FUNCTION__, result);
#ifdef TEST_HCI_CONTROL
    wiced_transport_send_data(HCI_CONTROL_ANCS_EVENT_DISCONNECTED, &result, 1);
#endif
    ancs_app_state.started = WICED_FALSE;
}


