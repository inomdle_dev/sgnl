/*
 * Copyright 2016, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

/** @file
 *
 *  This file implements the handsfree application finite state machine
 *
 */
#include "wiced_bt_sco.h"
#include "wiced_bt_hfp_hf.h"
#include "handsfree.h"

/*****************************************************************************
**  Constants
*****************************************************************************/
#define HANDSFREE_STATE_IDLE                0
#define HANDSFREE_STATE_DISCOVERABLE        1
#define HANDSFREE_STATE_CONNECTING          2
#define HANDSFREE_STATE_SLC_CONNECTED       3
#define HANDSFREE_STATE_INCOMING_CALL       4
#define HANDSFREE_STATE_IN_CALL             5


/*****************************************************************************
**  Variables
*****************************************************************************/
wiced_bt_voice_path_setup_t handsfree_sco_path =
{
    .path = WICED_BT_SCO_OVER_I2SPCM,
    .p_sco_data_cb = NULL
};

wiced_bt_sco_params_t handsfree_esco_params =
{
    0x000A,             /* Latency: 10 ms (HS/HF can use EV3, 2-EV3, 3-EV3) (S3) */
    HANDS_FREE_SCO_PKT_TYPES,
    BTM_ESCO_RETRANS_POWER, /* Retrans Effort (At least one retrans, opt for power) (S3) */
#if (WICED_BT_HFP_HF_WBS_INCLUDED == TRUE)
    WICED_TRUE
#else
    WICED_FALSE
#endif
};

//wiced_bt_sco_params_t handsfree_esco_params;

/*****************************************************************************
**  Functions
*****************************************************************************/
static void 			handsfree_hf_event_callback(wiced_bt_hfp_hf_event_t event, wiced_bt_hfp_hf_event_data_t* p_data);
void 					handsfree_hf_sco_management_callback(wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data);
//wiced_bt_dev_status_t wiced_bt_sco_create_as_acceptor (uint16_t *p_sco_index);
static void			handsfreee_hf_send_at_cmd(uint16_t handle, char *cmd, uint8_t arg_type, uint8_t arg_format, const char *p_arg, int16_t int_arg);


handsfree_app_state_t handsfree_app_state;


handsfree_app_state_t* get_context_ptr_from_handle(uint16_t handle)
{
    int i = 0;
    handsfree_app_state_t* tmp_ctxt = NULL;

    if (handsfree_app_state.rfcomm_handle == handle)
    {
        return &handsfree_app_state;
    }
    if (handsfree_app_state.connection_status == WICED_BT_HFP_HF_STATE_DISCONNECTED && (!tmp_ctxt))
    {
        tmp_ctxt = &handsfree_app_state;
    }
    return tmp_ctxt;
}

handsfree_app_state_t* get_context_ptr_from_sco_index(uint16_t sco_index)
{
    if (sco_index == HANDS_FREE_INVALID_SCO_INDEX)
    {
        return NULL;
    }
    if (sco_index == handsfree_app_state.sco_index && handsfree_app_state.connection_status != WICED_BT_HFP_HF_STATE_DISCONNECTED)
    {
        return &handsfree_app_state;
    }
    return NULL;
}

static void handsfree_hf_init_state(void)
{
    handsfree_app_state.call_active         = 0;
    handsfree_app_state.call_held           = 0;
    handsfree_app_state.call_setup          = WICED_BT_HFP_HF_CALLSETUP_STATE_IDLE;
    handsfree_app_state.connection_status   = WICED_BT_HFP_HF_STATE_DISCONNECTED;
    handsfree_app_state.spkr_volume         = 8;
    handsfree_app_state.mic_volume          = 8;
    handsfree_app_state.sco_index           = HANDS_FREE_INVALID_SCO_INDEX;
    handsfree_app_state.rfcomm_handle       = 0;
}

/*
 * Initialize HF unit.  If bda parameter is not NULL, it is the address of paired host,
 * start the connection.
 */
void handsfree_hf_init(BD_ADDR bda)
{
    wiced_result_t                result = WICED_BT_ERROR;
    wiced_bt_hfp_hf_config_data_t config;

    handsfree_hf_init_state();

    config.feature_mask     = HANDS_FREE_SUPPORTED_FEATURES;
    config.speaker_volume   = handsfree_app_state.spkr_volume;
    config.mic_volume       = handsfree_app_state.mic_volume;
    config.num_server       = 2;
    config.scn[0]           = HANDS_FREE_SCN;
    config.scn[1]           = HANDS_FREE_SCN;

    result = wiced_bt_hfp_hf_init(&config, handsfree_hf_event_callback);
    WICED_BT_TRACE("[%s] wiced_bt_hfp_hf_init %d\n\r",__func__, result);

//    Default mode is set to I2S Master so no need to call this API
//    To change the mode please call below API and to update PCM configuration use wiced_hal_set_pcm_config API
//    result = wiced_bt_sco_setup_voice_path(&handsfree_sco_path);
//    WICED_BT_TRACE("[%s] SCO Setting up voice path = %d\n\r",__func__, result);

    if (bda)
    {
        memcpy(handsfree_app_state.peer_bd_addr, bda, BD_ADDR_LEN);
        wiced_bt_hfp_hf_connect(handsfree_app_state.peer_bd_addr);
    }
}

/*
 * Process SCO management callback
 */
void handsfree_hf_sco_management_callback(wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data)
{
    uint16_t sco_index = 0xFFFF;
    uint16_t handle = 0xFFFF;
    handsfree_app_state_t* hfp_ptr = NULL;
    int status;
    switch (event)
    {
        case BTM_SCO_CONNECTED_EVT:             /**< SCO connected event. Event data: #wiced_bt_sco_connected_t */
            sco_index = p_event_data->sco_connected.sco_index;
            hfp_ptr = get_context_ptr_from_sco_index(sco_index);
            if (!hfp_ptr)
            {
                WICED_BT_TRACE("Invalid sco index in BTM_SCO_CONNECTED_EVT\n\r");
                break;
            }
            WICED_BT_TRACE("%s: SCO Audio connected, sco_index = %d [rfcomm_handle :%d]\n", __func__, sco_index, hfp_ptr->rfcomm_handle);

            break;

        case BTM_SCO_DISCONNECTED_EVT:          /**< SCO disconnected event. Event data: #wiced_bt_sco_disconnected_t */
            sco_index = p_event_data->sco_disconnected.sco_index;
            hfp_ptr = get_context_ptr_from_sco_index(sco_index);
            if (!hfp_ptr)
            {
                WICED_BT_TRACE("Invalid sco index in BTM_SCO_DISCONNECTED_EVT\n\r");
                break;
            }
            WICED_BT_TRACE("%s: SCO disconnection change event handler\n\r", __func__);

            status = wiced_bt_sco_create_as_acceptor(&hfp_ptr->sco_index);
            WICED_BT_TRACE("%s: status [%d] SCO INDEX [%d] \n", __func__, status, hfp_ptr->sco_index);
            break;

        case BTM_SCO_CONNECTION_REQUEST_EVT:    /**< SCO connection request event. Event data: #wiced_bt_sco_connection_request_t */
            hfp_ptr = get_context_ptr_from_sco_index(p_event_data->sco_connection_request.sco_index);
            if (!hfp_ptr)
            {
                WICED_BT_TRACE("Can't handle SCO_CONNECTION_REQUEST from <%B>\n", p_event_data->sco_connection_request.bd_addr);
                break;
            }

            hfp_ptr->sco_index = p_event_data->sco_connection_request.sco_index;

            WICED_BT_TRACE("%s: SCO connection request event handler \n", __func__);

            // ToDO check params
            wiced_bt_sco_accept_connection(hfp_ptr->sco_index, HCI_SUCCESS, (wiced_bt_sco_params_t *) &handsfree_esco_params);
            break;

        case BTM_SCO_CONNECTION_CHANGE_EVT:     /**< SCO connection change event. Event data: #wiced_bt_sco_connection_change_t */
            WICED_BT_TRACE("%s: SCO connection change event handler\n\r", __func__);
            break;
    }
}

static void handsfree_connection_event_handler(wiced_bt_hfp_hf_event_data_t* p_data)
{
    wiced_bt_dev_status_t status;
    handsfree_app_state_t* hfp_ptr = NULL;

    hfp_ptr = get_context_ptr_from_handle(p_data->handle);
    if (!hfp_ptr)
    {
        WICED_BT_TRACE("Can't get HFP context pointer for address: <%B>", p_data->conn_data.remote_address);
        return;
    }

    if (p_data->conn_data.conn_state == WICED_BT_HFP_HF_STATE_CONNECTED)
    {
        wiced_bt_hfp_hf_scb_t *p_scb = wiced_bt_hfp_hf_get_scb_by_bd_addr(p_data->conn_data.remote_address);
        memcpy(hfp_ptr->peer_bd_addr, p_data->conn_data.remote_address,BD_ADDR_LEN);
        hfp_ptr->rfcomm_handle = p_scb->rfcomm_handle;

        status = wiced_bt_sco_create_as_acceptor(&hfp_ptr->sco_index);

        WICED_BT_TRACE("%s: status [%d] SCO INDEX [%d] \n", __func__, status, hfp_ptr->sco_index);
    }
    else if (p_data->conn_data.conn_state == WICED_BT_HFP_HF_STATE_SLC_CONNECTED)
    {
        WICED_BT_TRACE("%s: Peer BD Addr [%B]\n", __func__, p_data->conn_data.remote_address);

        memcpy(hfp_ptr->peer_bd_addr, p_data->conn_data.remote_address, sizeof(wiced_bt_device_address_t));

#ifdef SGNL_SUPPORT_A2DP
        if (hfp_ptr->is_originator)
        {
            handsfree_av_connect(p_data->conn_data.remote_address);
        }
#endif		
    }
    else if (p_data->conn_data.conn_state == WICED_BT_HFP_HF_STATE_DISCONNECTED)
    {
        WICED_BT_TRACE("%s: disconnected\n\r", __func__);

        memset(hfp_ptr->peer_bd_addr, 0, sizeof(wiced_bt_device_address_t));
        if (hfp_ptr->sco_index != HANDS_FREE_INVALID_SCO_INDEX)
        {
            status = wiced_bt_sco_remove(hfp_ptr->sco_index);
            hfp_ptr->sco_index = HANDS_FREE_INVALID_SCO_INDEX;
            WICED_BT_TRACE("%s: remove sco status [%d] \n", __func__, status);
        }
        if (hfp_ptr->rfcomm_handle == 0)
        {
            device_connection_status_callback(p_data->conn_data.remote_address, NULL, WICED_FALSE, 0, BT_TRANSPORT_BR_EDR, 0);
        }
    }

    hfp_ptr->connection_status =  p_data->conn_data.conn_state;
}

static void handsfree_call_setup_event_handler(uint16_t handle, wiced_bt_hfp_hf_call_data_t* call_data)
{
    handsfree_app_state_t* hfp_ptr = NULL;
    hfp_ptr = get_context_ptr_from_handle(handle);

    if (!hfp_ptr)
    {
        WICED_BT_TRACE("Invalid remote-address or Can't handle call-setup\n\r");
        return;
    }

    switch (call_data->setup_state)
    {
        case WICED_BT_HFP_HF_CALLSETUP_STATE_INCOMING:
            // TODO: Add code to MCU wake up!!!
            break;

        case WICED_BT_HFP_HF_CALLSETUP_STATE_IDLE:
            if (call_data->active_call_present == 0)
            {
                if (hfp_ptr->call_setup == WICED_BT_HFP_HF_CALLSETUP_STATE_INCOMING ||
                        hfp_ptr->call_setup == WICED_BT_HFP_HF_CALLSETUP_STATE_DIALING ||
                        hfp_ptr->call_setup == WICED_BT_HFP_HF_CALLSETUP_STATE_ALERTING)
                {
                    WICED_BT_TRACE("Call: Inactive; Call Set-up: IDLE\n\r");
                    break;
                }
                /* If previous context has an active-call and active_call_present is 0 */
                if (hfp_ptr->call_active == 1)
                {
                    WICED_BT_TRACE("Call Terminated\n\r");
                    break;
                }
            }
            else if (call_data->active_call_present == 1)
            {
                WICED_BT_TRACE("Call: Active; Call-setup: DONE\n\r");
            }
            break;

        case WICED_BT_HFP_HF_CALLSETUP_STATE_DIALING:
            WICED_BT_TRACE("Call(outgoing) setting-up\n\r");
            break;

        case WICED_BT_HFP_HF_CALLSETUP_STATE_ALERTING:
            WICED_BT_TRACE("Remote(outgoing) ringing\n\r");
            break;

        default:
            break;
    }
    hfp_ptr->call_active = call_data->active_call_present;
    hfp_ptr->call_setup  = call_data->setup_state;
    hfp_ptr->call_held   = call_data->held_call_present;
}

static void handsfree_hf_event_callback(wiced_bt_hfp_hf_event_t event, wiced_bt_hfp_hf_event_data_t* p_data)
{
    int res = 0;
    handsfree_app_state_t* hfp_ptr = NULL;

    switch(event)
    {
    case WICED_BT_HFP_HF_CONNECTION_STATE_EVT:
        handsfree_connection_event_handler(p_data);
        break;

    case WICED_BT_HFP_HF_AG_FEATURE_SUPPORT_EVT:
        hfp_ptr = get_context_ptr_from_handle(p_data->handle);
        if (!hfp_ptr)
        {
            WICED_BT_TRACE("Invalid remote-address for AG_FEATURE_SUPPORT_EVENT\n\r");
            return;
        }

        if (hfp_ptr && (p_data->ag_feature_flags & WICED_BT_HFP_AG_FEATURE_INBAND_RING_TONE_CAPABILITY))
        {
            hfp_ptr->inband_ring_status = WICED_BT_HFP_HF_INBAND_RING_ENABLED;
        }
        else
        {
            hfp_ptr->inband_ring_status = WICED_BT_HFP_HF_INBAND_RING_DISABLED;
        }
#if (WICED_BT_HFP_HF_WBS_INCLUDED == TRUE)
        {
            wiced_bt_hfp_hf_scb_t    *p_scb = wiced_bt_hfp_hf_get_scb_by_handle(p_data->handle);
            if ((p_data->ag_feature_flags & WICED_BT_HFP_AG_FEATURE_CODEC_NEGOTIATION) &&
                    (p_scb->feature_mask & WICED_BT_HFP_AG_FEATURE_CODEC_NEGOTIATION))
            {
                handsfree_esco_params.use_wbs = WICED_TRUE;
            }
            else
            {
                handsfree_esco_params.use_wbs = WICED_FALSE;
            }
        }
#endif
        break;

    case WICED_BT_HFP_HF_SERVICE_STATE_EVT:
        WICED_BT_TRACE("Service State:%d\n\r", p_data->service_state);
        break;

    case WICED_BT_HFP_HF_CALL_SETUP_EVT:
        hfp_ptr = get_context_ptr_from_handle(p_data->handle);

        if (hfp_ptr->call_active != p_data->call_data.active_call_present)
            WICED_BT_TRACE("Active Call Present:%d\n\r", p_data->call_data.active_call_present);

        if (hfp_ptr->call_held != p_data->call_data.held_call_present)
            WICED_BT_TRACE("Held Call Present:%d\n\r", p_data->call_data.held_call_present);

        if (hfp_ptr->call_setup != p_data->call_data.setup_state)
            WICED_BT_TRACE("Call Setup State:%d\n\r", p_data->call_data.setup_state);

        handsfree_call_setup_event_handler(p_data->handle, &p_data->call_data);
        if (hfp_ptr->call_setup == WICED_BT_HFP_HF_CALLSETUP_STATE_INCOMING )
        {
			WICED_BT_TRACE("%s: CLIP - number %s, type %d\n\r", __func__, p_data->clip.caller_num, p_data->clip.type);
			// TODO: Add code, matching p_data->clip.caller_num to LED(index)
			// TODO: Decide MCU Send timing
        }
        break;

    case WICED_BT_HFP_HF_RSSI_IND_EVT:
        WICED_BT_TRACE("RSSI Ind:%d\n\r", p_data->rssi);
        break;

    case WICED_BT_HFP_HF_SERVICE_TYPE_EVT:
        WICED_BT_TRACE("Roam Ind:%d\n\r", p_data->service_type);
        break;

    case WICED_BT_HFP_HF_BATTERY_STATUS_IND_EVT:
        WICED_BT_TRACE("Battery Ind:%d\n\r", p_data->battery_level);
        break;

    case WICED_BT_HFP_HF_RING_EVT:
        WICED_BT_TRACE("RING\n\r");
        break;

    case WICED_BT_HFP_HF_INBAND_RING_STATE_EVT:
        hfp_ptr = get_context_ptr_from_handle(p_data->handle);
        if (!hfp_ptr)
        {
            WICED_BT_TRACE("Invalid remote-address for INBAND_RING_STATE_EVENT\n\r");
            return;
        }
        hfp_ptr->inband_ring_status = p_data->inband_ring;
        break;

    case WICED_BT_HFP_HF_CLIP_IND_EVT:
        WICED_BT_TRACE("%s: CLIP - number %s, type %d\n\r", __func__, p_data->clip.caller_num, p_data->clip.type);
        break;

    case WICED_BT_HFP_HF_VOLUME_CHANGE_EVT:
        WICED_BT_TRACE("%s: %s VOLUME - %d \n", __func__, (p_data->volume.type == WICED_BT_HFP_HF_SPEAKER) ? "SPK" : "MIC",  p_data->volume.level);
        break;

    case WICED_BT_HFP_HFP_CODEC_SET_EVT:
        handsfree_esco_params.use_wbs = (p_data->selected_codec == WICED_BT_HFP_HF_MSBC_CODEC) ? WICED_TRUE : WICED_FALSE;
        break;

    default:
        break;
    }
}

void handsfree_hf_button_push_event_handler(void)
{
    int res = 0;
    handsfree_app_state_t* hfp_ptr = &handsfree_app_state;

    WICED_BT_TRACE("Button connection handle:%d status:%d call active:%d held:%d call_setup:%d\n\r",
            hfp_ptr->rfcomm_handle, hfp_ptr->connection_status, hfp_ptr->call_active, handsfree_app_state.call_held, hfp_ptr->call_setup);

    if (hfp_ptr->connection_status == WICED_BT_HFP_HF_STATE_SLC_CONNECTED)
    {
        if (hfp_ptr->call_active == 0)
        {
            switch (hfp_ptr->call_setup)
            {
            case WICED_BT_HFP_HF_CALLSETUP_STATE_IDLE:
//                handsfreee_hf_send_at_cmd(hfp_ptr->rfcomm_handle, "+BVRA", WICED_BT_HFP_HF_AT_SET, WICED_BT_HFP_HF_AT_FMT_INT, NULL, 1);
                return;

            case WICED_BT_HFP_HF_CALLSETUP_STATE_INCOMING:
                wiced_bt_hfp_hf_perform_call_action(hfp_ptr->rfcomm_handle, WICED_BT_HFP_HF_CALL_ACTION_ANSWER, NULL);
                return;

            case WICED_BT_HFP_HF_CALLSETUP_STATE_DIALING:
            case WICED_BT_HFP_HF_CALLSETUP_STATE_ALERTING:
                wiced_bt_hfp_hf_perform_call_action(hfp_ptr->rfcomm_handle, WICED_BT_HFP_HF_CALL_ACTION_HANGUP, NULL);
                return;
            }
        }
        else
        {
            if (handsfree_app_state.call_held == 0)
            {
                wiced_bt_hfp_hf_perform_call_action(hfp_ptr->rfcomm_handle, WICED_BT_HFP_HF_CALL_ACTION_HANGUP, NULL);
                return;
            }
            else
            {
                wiced_bt_hfp_hf_perform_call_action(hfp_ptr->rfcomm_handle, WICED_BT_HFP_HF_CALL_ACTION_HOLD_2, NULL);
                return;
            }
        }
    }
    WICED_BT_TRACE("button not processed\n\r");
}


void handsfree_hf_button_long_push_event_handler(void)
{
    int res = 0;
    handsfree_app_state_t* hfp_ptr = &handsfree_app_state;

    WICED_BT_TRACE("Long Button connection handle:%d status:%d call active:%d held:%d call_setup:%d\n\r",
        hfp_ptr->rfcomm_handle, hfp_ptr->connection_status, hfp_ptr->call_active, handsfree_app_state.call_held, hfp_ptr->call_setup);

    if (hfp_ptr->connection_status == WICED_BT_HFP_HF_STATE_SLC_CONNECTED)
    {
        if (hfp_ptr->call_active == 0)
        {
            switch (hfp_ptr->call_setup)
            {
            case WICED_BT_HFP_HF_CALLSETUP_STATE_IDLE:
            	wiced_bt_hfp_hf_perform_call_action(hfp_ptr->rfcomm_handle, WICED_BT_HFP_HF_CALL_ACTION_DIAL, NULL);
                return;

            case WICED_BT_HFP_HF_CALLSETUP_STATE_INCOMING:
            case WICED_BT_HFP_HF_CALLSETUP_STATE_DIALING:
            case WICED_BT_HFP_HF_CALLSETUP_STATE_ALERTING:
                wiced_bt_hfp_hf_perform_call_action(hfp_ptr->rfcomm_handle, WICED_BT_HFP_HF_CALL_ACTION_HANGUP, NULL);
                return;
            }
        }
        else
        {
            if (handsfree_app_state.call_held == 0)
            {
                wiced_bt_hfp_hf_perform_call_action(hfp_ptr->rfcomm_handle, WICED_BT_HFP_HF_CALL_ACTION_HANGUP, NULL);
                return;
            }
            else
            {
                wiced_bt_hfp_hf_perform_call_action(hfp_ptr->rfcomm_handle, WICED_BT_HFP_HF_CALL_ACTION_HOLD_2, NULL);
                return;
            }
        }
    }
    WICED_BT_TRACE("button not processed\n\r");
}

static void handsfreee_hf_send_at_cmd (uint16_t handle, char *cmd, uint8_t arg_type, uint8_t arg_format, const char *p_arg, int16_t int_arg)
{
    char    buf[WICED_BT_HFP_HF_AT_MAX_LEN + 16];
    char    *p = buf;

    memset (buf, 0, (WICED_BT_HFP_HF_AT_MAX_LEN+16));

    *p++ = 'A';
    *p++ = 'T';

    /* copy result code string */
    memcpy(p,cmd, strlen(cmd));
    p += strlen(cmd);

    if(arg_type == WICED_BT_HFP_HF_AT_SET)
    {
        *p++ = '=';

    }
    else if(arg_type == WICED_BT_HFP_HF_AT_READ)
    {
        *p++ = '?';

    }
    else if(arg_type == WICED_BT_HFP_HF_AT_TEST)
    {
        *p++ = '=';
        *p++ = '?';

    }

    /* copy argument if any */
    if (arg_format == WICED_BT_HFP_HF_AT_FMT_INT)
    {
        p += util_itoa((uint16_t) int_arg, p);
    }
    else if (arg_format == WICED_BT_HFP_HF_AT_FMT_STR)
    {
        utl_strcpy(p, p_arg);
        p += strlen(p_arg);
    }

    /* finish with \r*/
    *p++ = '\r';

    wiced_bt_hfp_hf_send_at_cmd(handle, buf);
}

