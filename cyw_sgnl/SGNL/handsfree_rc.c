/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * This file implements audio application controlled over UART.
 *
 */
#include "bt_types.h"
#include "wiced_bt_sdp.h"
#include "wiced_result.h"
#include "wiced_bt_dev.h"
#include "wiced_bt_avrc.h"
#include "wiced_bt_trace.h"
#include "wiced_bt_cfg.h"
#include "handsfree.h"
#include "wiced_bt_avrc_ct.h"

/******************************************************************************
 *                          Constants
 ******************************************************************************/
#define AVCT_MIN_CONTROL_MTU                        48      /* Per the AVRC spec, minimum MTU for the control channel */
#define AVCT_CONTROL_MTU                            256     /* MTU for the control channel */
#define AVCT_MIN_BROWSE_MTU                         335     /* Per the AVRC spec, minimum MTU for the browsing channel */

#define SDP_DB_LEN                                  400

#define sizeof_array(a) (sizeof(a)/sizeof(a[0]))
#define CASE_RETURN_STR(const) case const: return #const;

#define MAX_AVRCP_VOLUME_LEVEL  0x7f

/******************************************************************************
 *                         Variable Definitions
 ******************************************************************************/

typedef struct
{
    uint16_t handle;
    wiced_bt_device_address_t remote_addr;
    wiced_bt_avrc_ct_connection_state_t connection_state;
    uint8_t  play_status;
} tRC_APP_CB;

static tRC_APP_CB rc_app_cb;

uint8_t avrc_supported_events[] =
{
    FALSE,
    TRUE,            /* AVRC_EVT_PLAY_STATUS_CHANGE             0x01    /**< Playback Status Changed */
    FALSE,           /* AVRC_EVT_TRACK_CHANGE                   0x02    /**< Track Changed */
    FALSE,           /* AVRC_EVT_TRACK_REACHED_END              0x03    /**< Track End Reached */
    FALSE,           /* AVRC_EVT_TRACK_REACHED_START            0x04    /**< Track Reached Start */
    FALSE,           /* AVRC_EVT_PLAY_POS_CHANGED               0x05    /**< Playback position changed */
    FALSE,           /* AVRC_EVT_BATTERY_STATUS_CHANGE          0x06    /**< Battery status changed */
    FALSE,           /* AVRC_EVT_SYSTEM_STATUS_CHANGE           0x07    /**< System status changed */
    FALSE,           /* AVRC_EVT_APP_SETTING_CHANGE             0x08    /**< Player application settings changed */
    FALSE,           /* AVRC_EVT_NOW_PLAYING_CHANGE             0x09    /**< Now Playing Content Changed (AVRCP 1.4) */
    FALSE,           /* AVRC_EVT_AVAL_PLAYERS_CHANGE            0x0a    /**< Available Players Changed Notification (AVRCP 1.4) */
    FALSE,           /* AVRC_EVT_ADDR_PLAYER_CHANGE             0x0b    /**< Addressed Player Changed Notification (AVRCP 1.4) */
    FALSE,           /* AVRC_EVT_UIDS_CHANGE                    0x0c    /**< UIDs Changed Notification (AVRCP 1.4) */
    FALSE            /* AVRC_EVT_VOLUME_CHANGE                  0x0d    /**< Notify Volume Change (AVRCP 1.4) */
};

/* Map avrc status values to the most appropriate WICED result */
wiced_result_t avrc_status_to_wiced[] =
{
    WICED_ERROR,            /* #define AVRC_STS_BAD_CMD        0x00    /**< Invalid command, sent if TG received a PDU that it did not understand. */
    WICED_BADARG,           /* #define AVRC_STS_BAD_PARAM      0x01    /**< Invalid parameter, sent if the TG received a PDU with a parameter ID that it did not understand. Sent if there is only one parameter ID in the PDU. */
    WICED_BADOPTION,        /* #define AVRC_STS_NOT_FOUND      0x02    /**< Specified parameter not found., sent if the parameter ID is understood, but content is wrong or corrupted. */
    WICED_ERROR,            /* #define AVRC_STS_INTERNAL_ERR   0x03    /**< Internal Error, sent if there are error conditions not covered by a more specific error code. */
    WICED_SUCCESS,          /* #define AVRC_STS_NO_ERROR       0x04    /**< Operation completed without error.  This is the status that should be returned if the operation was successful. */
    WICED_BADARG,           /* #define AVRC_STS_UID_CHANGED    0x05    /**< UID Changed - The UIDs on the device have changed */
    WICED_ERROR,            /* #define AVRC_STS_GEN_ERROR      0x06    Unknown Error - this is changed to "reserved" */
    WICED_ERROR,            /* #define AVRC_STS_BAD_DIR        0x07    /**< Invalid Direction - The Direction parameter is invalid - Change Path*/
    WICED_ERROR,            /* #define AVRC_STS_NOT_DIR        0x08    /**< Not a Directory - The UID provided does not refer to a folder item  Change Path*/
    WICED_NOT_FOUND,        /* #define AVRC_STS_NOT_EXIST      0x09    /**< Does Not Exist - The UID provided does not refer to any item    Change Path, PlayItem, AddToNowPlaying, GetItemAttributes*/
    WICED_BADVALUE,         /* #define AVRC_STS_BAD_SCOPE      0x0a    /**< Invalid Scope - The scope parameter is invalid  GetFolderItems, PlayItem, AddToNowPlayer, GetItemAttributes, */
    WICED_BADVALUE,         /* #define AVRC_STS_BAD_RANGE      0x0b    /**< Range Out of Bounds - The start of range provided is not valid  GetFolderItems*/
    WICED_BADVALUE,         /* #define AVRC_STS_UID_IS_DIR     0x0c    /**< UID is a Directory - The UID provided refers to a directory, which cannot be handled by this media player   PlayItem, AddToNowPlaying */
    WICED_ERROR,            /* #define AVRC_STS_IN_USE         0x0d    /**< Media in Use - The media is not able to be used for this operation at this time PlayItem, AddToNowPlaying */
    WICED_ERROR,            /* #define AVRC_STS_NOW_LIST_FULL  0x0e    /**< Now Playing List Full - No more items can be added to the Now Playing List  AddToNowPlaying*/
    WICED_UNSUPPORTED,      /* #define AVRC_STS_SEARCH_NOT_SUP 0x0f    /**< Search Not Supported - The Browsed Media Player does not support search Search */
    WICED_ERROR,            /* #define AVRC_STS_SEARCH_BUSY    0x10    /**< Search in Progress - A search operation is already in progress  Search*/
    WICED_ERROR,            /* #define AVRC_STS_BAD_PLAYER_ID  0x11    /**< Invalid Player Id - The specified Player Id does not refer to a valid player    SetAddressedPlayer, SetBrowsedPlayer*/
    WICED_UNSUPPORTED,      /* #define AVRC_STS_PLAYER_N_BR    0x12    /**< Player Not Browsable - The Player Id supplied refers to a Media Player which does not support browsing. SetBrowsedPlayer */
    WICED_ERROR,            /* #define AVRC_STS_PLAYER_N_ADDR  0x13    /**< Player Not Addressed.  The Player Id supplied refers to a player which is not currently addressed, and the command is not able to be performed if the player is not set as addressed.   Search, SetBrowsedPlayer*/
    WICED_ERROR,            /* #define AVRC_STS_BAD_SEARCH_RES 0x14    /**< No valid Search Results - The Search result list does not contain valid entries, e.g. after being invalidated due to change of browsed player   GetFolderItems */
    WICED_ERROR,            /* #define AVRC_STS_NO_AVAL_PLAYER 0x15    /**< No available players ALL */
    WICED_ERROR             /* #define AVRC_STS_ADDR_PLAYER_CHG 0x16   /**< Addressed Player Changed - Register Notification */
};

#define avrc_status_to_wiced_result(a) ((a<=AVRC_STS_ADDR_PLAYER_CHG) ? avrc_status_to_wiced[a] : WICED_ERROR)

/******************************************************************************
 *                          Function Definitions
 ******************************************************************************/

static uint16_t get_connection_handle_from_address(wiced_bt_device_address_t bd_addr)
{
    int i = 0;
    uint16_t idle_index = 0xFFFF;

    if (bdcmp(bd_addr, rc_app_cb.remote_addr) == 0)
    {
        return i;
    }
    if (rc_app_cb.connection_state == REMOTE_CONTROL_DISCONNECTED && idle_index == 0xFFFF)
    {
        idle_index = i;
    }
    return idle_index;
}


static uint16_t get_index_from_handle(uint8_t handle)
{
    int i = 0;
    uint16_t idle_index = 0xFFFF;

    if (handle == rc_app_cb.handle)
    {
        return i;
    }
    return idle_index;
}

/**
 *
 * Function         avrc_app_hci_pass_through
 *
 *                  AVRC Passthrough command requests from the MCU
 *                  are ... well ... passed through to the AVRCP api.
 *
 * @param[in]       op_id    : Pass through command id (see #AVRC_ID_XX)
 * @param[in]       state    : State of the pass through command (see #AVRC_STATE_XX)
 *
 * @return          wiced_result_t
 */
wiced_result_t avrc_app_hci_pass_through (uint16_t handle, uint8_t op_id, uint8_t state )
{
    wiced_result_t result = WICED_NOT_CONNECTED;
    tRC_APP_CB*    ptr_cb = &rc_app_cb;

    WICED_BT_TRACE("%s op_id:%d state:%d\n\r",__FUNCTION__, op_id, state);
    /* Make sure there is a connection before trying to send a command */
    if (ptr_cb->connection_state == REMOTE_CONTROL_CONNECTED)
    {
        result = wiced_bt_avrc_ct_send_pass_through_cmd(handle, op_id, state, 0, NULL);
    }

    return result;
}

/*
 * Callback invoked by the AVRCP api when a connection status change has occurred.
 */
void avrc_connection_state_cback(uint8_t handle, wiced_bt_device_address_t remote_addr, wiced_result_t status,
                                  wiced_bt_avrc_ct_connection_state_t connection_state,
                                  uint32_t peer_features)
{
    tRC_APP_CB* ptr_cb = &rc_app_cb;

    WICED_BT_TRACE("[%s]: %s (%d): <%B> features: 0x%x Handle:%d\n\r", __FUNCTION__,
                    (connection_state == REMOTE_CONTROL_DISCONNECTED) ? "REMOTE_CONTROL_DISCONNECTED" :
                    (connection_state == REMOTE_CONTROL_CONNECTED)    ? "REMOTE_CONTROL_CONNECTED" :
                    (connection_state == REMOTE_CONTROL_INITIALIZED)  ? "REMOTE_CONTROL_INITIALIZED" :
                                                                        "INVALID STATE",
                    connection_state,
                    remote_addr, peer_features, handle);

    /* Service the connection state change. */
    switch (connection_state)
    {
    case REMOTE_CONTROL_DISCONNECTED:
        ptr_cb->connection_state = REMOTE_CONTROL_DISCONNECTED;
        break;

    case REMOTE_CONTROL_CONNECTED:
        /* Save the peer address for other functions. */
        memcpy(ptr_cb->remote_addr, remote_addr, BD_ADDR_LEN);
        ptr_cb->connection_state = REMOTE_CONTROL_CONNECTED;
        ptr_cb->handle = handle;

        if (handsfree_app_state.is_originator)
        {
            handsfree_app_state.is_originator = WICED_FALSE;
            avrc_app_hci_pass_through (handle, AVRC_ID_PLAY, AVRC_STATE_PRESS);
        }
        break;

    case REMOTE_CONTROL_INITIALIZED:
        /* Find out what app controls the player has to offer. */
        wiced_bt_avrc_ct_list_player_attrs_cmd(handle);
        break;
    }
}

/**
 *
 * Callback invoked by the avrc_response_cback to inform of a registered event being sent from the peer.
 */
void avrc_handle_registered_notification_rsp(uint8_t handle, wiced_bt_avrc_response_t *avrc_rsp)
{
    wiced_bt_avrc_reg_notif_rsp_t *reg_notif = (wiced_bt_avrc_reg_notif_rsp_t *)avrc_rsp;

    switch (reg_notif->event_id)
    {
    case AVRC_EVT_PLAY_STATUS_CHANGE:             // Playback Status Changed
        rc_app_cb.play_status = reg_notif->param.play_status;
        break;

    default:
        WICED_BT_TRACE("[%s]: unhandled event: Event ID: 0x%x\n\r", __FUNCTION__, reg_notif->event_id);
        break;
    }
}

/*
 * Callback invoked by avrc_response_cback to inform of a response to the get play status request.
 */
void avrc_handle_get_play_status_rsp(uint8_t handle, wiced_bt_avrc_response_t *avrc_rsp)
{
    wiced_bt_avrc_get_play_status_rsp_t* get_play_status_val = &avrc_rsp->get_play_status;

    WICED_BT_TRACE("[%s]: handle: <%d> PDU: 0x%x cmd_status:%x play-status:%x\n\r", __FUNCTION__,  handle, avrc_rsp->pdu,
                    get_play_status_val->status, get_play_status_val->play_status);

    rc_app_cb.play_status = get_play_status_val->play_status;
}

/*
 * Callback invoked by the AVRCP api to inform of a response to an AVRCP request submitted or 
 * possibly an event that was registered on our behalf by the API.
 */
void avrc_response_cback(uint8_t handle,
                          wiced_bt_avrc_response_t *avrc_rsp)
{
    /* Check the status. If successful handle the response */
    switch (avrc_rsp->pdu)
    {
    case AVRC_PDU_REGISTER_NOTIFICATION:
        avrc_handle_registered_notification_rsp(handle, avrc_rsp);
        break;

    case AVRC_PDU_GET_PLAY_STATUS:
        avrc_handle_get_play_status_rsp(handle, avrc_rsp);
        break;

    default:
        WICED_BT_TRACE("[%s]: unhandled response: PDU: 0x%x\n\r", __FUNCTION__, avrc_rsp->pdu);
        break;
    }
}

/*
 * Callback invoked by the AVRCP api to inform of a command sent
 * from the peer.
 */
void avrc_command_cback(uint8_t handle, wiced_bt_avrc_command_t *avrc_cmd)
{
    switch (avrc_cmd->pdu)
    {
    case AVRC_PDU_SET_ABSOLUTE_VOLUME:
        // TODO AVRC set volume
        WICED_BT_TRACE("AVRC Volume set :%d%%\n", (uint8_t)((avrc_cmd->volume.volume * 100) / MAX_AVRCP_VOLUME_LEVEL));
        break;

    default:
        WICED_BT_TRACE("%s: Unsupported command callback PDU: 0x%x\n\r", __FUNCTION__,  avrc_cmd->pdu);
        break;
    }
}

/*
 * Callback invoked on completion of a passthrough command. If the
 * command had been a "PRESS" state command then the "RELEASE" is automatically
 * sent by this callback.
 */
void avrc_passthrough_cback(uint8_t handle, wiced_bt_avrc_msg_pass_t *avrc_pass_rsp)
{
    WICED_BT_TRACE("[%s]: handle: %d op_id: 0x%x\n\r", __FUNCTION__,  handle, avrc_pass_rsp->op_id);

    if (avrc_pass_rsp->hdr.ctype == AVRC_RSP_ACCEPT)
    {
        /* Assume that if the state of the keypress is "press" they will want to "release" */
        if (avrc_pass_rsp->state == AVRC_STATE_PRESS)
        {
            avrc_app_hci_pass_through(handle, avrc_pass_rsp->op_id, AVRC_STATE_RELEASE);
        }
    }
    else
    {
        WICED_BT_TRACE("%s: op_id: 0x%x failed: 0x%x\n\r", __FUNCTION__, avrc_pass_rsp->op_id, avrc_pass_rsp->hdr.ctype);
    }
}

/*
 * Initialize the AVRCP api as a controller and set the
 * necessary callbacks for it to invoke.
 */
wiced_result_t handsfree_rc_init(void)
{
    uint32_t local_features = REMOTE_CONTROL_FEATURE_CONTROLLER | REMOTE_CONTROL_FEATURE_TARGET;
    //uint32_t local_features = REMOTE_CONTROL_FEATURE_CONTROLLER;

    /* Clear the control block */
    memset(&rc_app_cb, 0, sizeof(tRC_APP_CB));

    /* Call library interface for avrc initialization */
    return wiced_bt_avrc_ct_init(local_features,
                                  avrc_supported_events,
                                  avrc_connection_state_cback,
                                  avrc_command_cback,
                                  avrc_response_cback,
                                  avrc_passthrough_cback);
}

/*
 * Process play/pause button
 */
void handsfree_rc_button_event_handler(void)
{
    if (rc_app_cb.play_status == AVRC_PLAYSTATE_PLAYING)
    {
        avrc_app_hci_pass_through (rc_app_cb.handle, AVRC_ID_PAUSE, AVRC_STATE_PRESS);
    }
    else
    {
        avrc_app_hci_pass_through (rc_app_cb.handle, AVRC_ID_PLAY, AVRC_STATE_PRESS);
    }
}

/*
 * Process next button
 */
void handsfree_rc_next_track_button_event_handler(void)
{
    avrc_app_hci_pass_through (rc_app_cb.handle, AVRC_ID_FORWARD, AVRC_STATE_PRESS);
}

/*
 * Process previous button
 */
void handsfree_rc_previous_track_button_event_handler(void)
{
    avrc_app_hci_pass_through (rc_app_cb.handle, AVRC_ID_BACKWARD, AVRC_STATE_PRESS);
}


