#ifndef __SGNL_H__
#define __SGNL_H__

/*******************************************************************
 * Constant Definitions
 ******************************************************************/
#define SGNL_APP_TIMEOUT_IN_SECONDS              1
#define SGNL_APP_FINE_TIMEOUT_IN_MILLISECONDS    12
#define TRANS_UART_BUFFER_SIZE                          1024
#define TRANS_UART_BUFFER_COUNT                         2

#define HANDSFREE_DISCOVERABLE_DURATION                 600
#define HANDSFREE_NVRAM_ID                              WICED_NVRAM_VSID_START
#define HANDSFREE_LOCAL_KEYS_VS_ID                      (WICED_NVRAM_VSID_START + 2)

#define SGNL_KEY_F_GPIO_PIN_BUTTON			(WICED_P33) // (WICED_P27)
#define SGNL_KEY_UP_GPIO_PIN_BUTTON			(WICED_P15)
#define SGNL_KEY_DN_GPIO_PIN_BUTTON			(WICED_P26)

/******************************************************
 *                     Structures
 ******************************************************/
typedef struct
{
#define LE_CONTROL_STATE_IDLE 							(0)
#define LE_CONTROL_STATE_DISCOVER_PRIMARY_SERVICES 	(1)
#define LE_CONTROL_STATE_DISCOVER_CHARACTERISTICS 	(2)
#define LE_CONTROL_STATE_DISCOVER_DESCRIPTORS			(3)
#define LE_CONTROL_STATE_READ_VALUE 					(4)
#define LE_CONTROL_STATE_WRITE_VALUE					(5)
#define LE_CONTROL_STATE_WRITE_NO_RESPONSE_VALUE		(6)
#define LE_CONTROL_STATE_NOTIFY_VALUE					(7)
#define LE_CONTROL_STATE_INDICATE_VALUE					(8)
#define LE_CONTROL_STATE_WRITE_DESCRIPTOR_VALUE		(9)
#define LE_CONTROL_STATE_DISCONNECTING					(10)

    uint8_t 			state;                // Application discovery state
    wiced_bool_t		indication_sent;      // TRUE if indication sent and not acked
    uint16_t			conn_id;              // Connection ID used for exchange with the stack
    BD_ADDR				bd_addr;
    uint16_t			peer_mtu;             // MTU received in the MTU request (or 23 if peer did not send MTU request)
    uint8_t				role;                 // HCI_ROLE_MASTER or HCI_ROLE_SLAVE
} ble_conn_state_t;

ble_conn_state_t ble_app_state;

typedef struct t_ble_pending_tx_buffer_t
{
    wiced_bool_t        tx_buf_saved;
    uint16_t            tx_buf_conn_id;
    uint16_t            tx_buf_type;
    uint16_t            tx_buf_len;
    uint16_t            tx_buf_handle;
    uint8_t             tx_buf_data[HCI_CONTROL_GATT_COMMAND_MAX_TX_BUFFER];
} ble_pending_tx_buffer_t;

ble_pending_tx_buffer_t ble_pending_tx_buffer;


/*******************************************************************
 * Function Prototypes-
 ******************************************************************/
static void 					sgnl_app_init( void );
static wiced_bt_dev_status_t 	sgnl_management_callback( wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data );
static void 					sgnl_set_advertisement_data( void );
static void 					sgnl_advertisement_stopped( void );
static void 					sgnl_interrupt_handler( void* data, uint8_t value );
static void 					sgnl_reset_device( void );
/* GATT Registration Callbacks */
static wiced_bt_gatt_status_t 	sgnl_gatt_connect_callback( wiced_bt_gatt_connection_status_t *p_conn_status );
static wiced_bt_gatt_status_t 	sgnl_gatt_op_complete_callback( wiced_bt_gatt_operation_complete_t *p_complete );
static wiced_bt_gatt_status_t 	sgnl_gatt_req_read_handler( wiced_bt_gatt_read_t *p_read_req, uint16_t conn_id );
static wiced_bt_gatt_status_t 	sgnl_gatt_req_write_handler( wiced_bt_gatt_write_t *p_write_req, uint16_t conn_id );
static wiced_bt_gatt_status_t 	sgnl_gatt_req_exec_write_handler( wiced_bt_gatt_exec_flag_t exec_flag, uint16_t conn_id );
static wiced_bt_gatt_status_t	sgnl_gatt_req_mtu_handler( uint16_t conn_id, uint16_t mtu );
static wiced_bt_gatt_status_t sgnl_gatt_req_conf_handler( uint16_t conn_id, uint16_t handle );
static wiced_bt_gatt_status_t 	sgnl_attr_req_callback(wiced_bt_gatt_attribute_request_t *p_req);
static wiced_bt_gatt_status_t 	sgnl_gatt_event_handler( wiced_bt_gatt_evt_t  event, wiced_bt_gatt_event_data_t *p_event_data );
static void 					sgnl_timeout(uint32_t count);
static void 					sgnl_fine_timeout(uint32_t finecount );
static uint32_t 				hci_control_process_rx_cmd( uint8_t* p_data, uint32_t len );
#ifdef HCI_TRACE_OVER_TRANSPORT
static void 					sgnl_trace_callback( wiced_bt_hci_trace_type_t type, uint16_t length, uint8_t* p_data );
#endif
static wiced_bool_t 			get_paired_host_info(uint8_t* bda);
static wiced_bool_t 			read_device_link_keys(wiced_bt_device_link_keys_t *p_keys);
static wiced_bool_t 			delete_device_link_keys(void);
static wiced_bool_t 			save_device_link_keys(wiced_bt_device_link_keys_t *p_keys);
static void 					load_device_keys_to_addr_resolution_db(void);
static wiced_bool_t 			is_device_bonded(BD_ADDR bd_addr);
static void 					set_discoverable(void);
static void 					write_eir(void);
void 							device_connection_status_callback(wiced_bt_device_address_t bd_addr, uint8_t *p_features, wiced_bool_t is_connected, uint16_t handle, wiced_bt_transport_t transport, uint8_t reason);
static void					key_event_handler(void);
void							user_init(void);
void 							function_key_process(void);
void 							power_key_process(void);
void							navigation_key_process(uint8_t is_up);
void							do_not_disturb_process(void);
void 							initialize_device_process(void);
static void 					discovery_timer_callback(uint32_t arg);
static wiced_result_t ble_connection_down( wiced_bt_gatt_connection_status_t *p_status );
static wiced_result_t ble_connection_up( wiced_bt_gatt_connection_status_t *p_status );
static void ble_notification_handler( uint16_t conn_id, uint16_t handle, uint8_t *p_data, uint16_t len );
static void ble_indication_handler( uint16_t conn_id, uint16_t handle, uint8_t *p_data, uint16_t len );
static void ble_send_write_completed( uint16_t con_handle, uint8_t result );
static void ble_send_gatt_write_error_event( uint16_t con_handle, uint8_t write_result );
static void ble_send_read_rsp( uint16_t con_handle, uint8_t *data, int len );
static void ble_send_gatt_read_error_event( uint16_t con_handle, uint8_t read_result );


/*******************************************************************
 * Macro Definitions
 ******************************************************************/
// Macro to extract uint16_t from little-endian byte array
#define LITTLE_ENDIAN_BYTE_ARRAY_TO_UINT16(byte_array) \
        (uint16_t)( ((byte_array)[0] | ((byte_array)[1] << 8)) )

/*******************************************************************
 * Transport Configuration
 ******************************************************************/
wiced_transport_cfg_t transport_cfg =
{
    .type =                         WICED_TRANSPORT_UART,                    /**< Wiced transport type. */
    /* Transport Interface Configuration */
    .cfg = {
        /* UART Transport Configuration */
        .uart_cfg = {
            .mode =                 WICED_TRANSPORT_UART_HCI_MODE,           /**<  UART mode, HCI or Raw */
            .baud_rate =            HCI_UART_MAX_BAUD                        /**<  UART baud rate */
        }
    },
    /* Rx Buffer Pool Configuration */
    .rx_buff_pool_cfg = {
        .buffer_size =              TRANS_UART_BUFFER_SIZE,                  /**<  Rx Buffer Size */
        .buffer_count =             TRANS_UART_BUFFER_COUNT                  /**<  Rx Buffer Count */
    },
    .p_status_handler =             NULL,                                    /**< Wiced transport status handler.*/
    .p_data_handler =               hci_control_process_rx_cmd,              /**< Wiced transport receive data handler. */
    .p_tx_complete_cback =          NULL                                     /**< Wiced transport tx complete callback. */
};


/*******************************************************************
 * Variable Definitions
 ******************************************************************/
static uint8_t  BTN_mode = 0;
static uint8_t  BTN_prev_mode = 0;
const wiced_bt_audio_config_buffer_t wiced_bt_audio_buf_config;
// Transport pool for sending RFCOMM data to host
static wiced_transport_buffer_pool_t* transport_pool = NULL;


#endif
