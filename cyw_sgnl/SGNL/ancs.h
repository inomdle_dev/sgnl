#ifndef _ANCS_H_
#define _ANCS_H_

#include "wiced_bt_ancs.h"

/******************************************************
 *                      Constants
 ******************************************************/

#ifdef WICED_BT_TRACE_ENABLE
static char *EventId[] =
{
    "Added",
    "Modified",
    "Removed",
    "Unknown"
};

#define ANCS_CATEGORY_ID_MAX    12
static char *CategoryId[] =
{
    "Other",
    "IncomingCall",
    "MissedCall",
    "Voicemail",
    "Social",
    "Schedule",
    "Email",
    "News",
    "HealthAndFitness",
    "BusinessAndFinance",
    "Location",
    "Entertainment",
    "Unknown"
};

static char *NotificationAttributeID[] =
{
    "AppIdentifier",
    "Title",
    "Subtitle",
    "Message",
    "MessageSize",
    "Date",
    "PositiveActLabel",
    "NegativeActLabel",
    "Unknown"
};
#endif

/******************************************************
 *                     Structures
 ******************************************************/

/******************************************************
 *               Function Prototypes
 ******************************************************/
extern void 					ancs_connection_up(wiced_bt_gatt_connection_status_t *p_conn_status);
extern void 					ancs_connection_down(wiced_bt_gatt_connection_status_t *p_conn_status);
extern void 					ancs_process_pairing_complete(wiced_bt_dev_pairing_cplt_t *pairing_complete);
extern wiced_bt_gatt_status_t	ancs_gatt_discovery_result(wiced_bt_gatt_discovery_result_t *p_data);
extern wiced_bt_gatt_status_t	ancs_gatt_discovery_complete(wiced_bt_gatt_discovery_complete_t *p_data);
static void                   ancs_discovery_complete_callback(uint16_t conn_id, wiced_bool_t result);
static void                   ancs_start_complete_callback(uint16_t conn_id, wiced_bt_gatt_status_t result);
static void                   ancs_stop_complete_callback(uint16_t conn_id, wiced_bt_gatt_status_t result);
static void                   ancs_notification_callback(uint16_t conn_id, ancs_event_t *p_ancs_event);


/******************************************************
 *               Variables Definitions
 ******************************************************/

#pragma pack(1)
// host information for NVRAM
typedef PACKED struct
{
    // BD address of the bonded host
    BD_ADDR  bdaddr;
}  HOSTINFO;

typedef struct
{
    uint16_t                    conn_id;

#define ANCS_DISCOVERY_STATE_SERVICE    0
#define ANCS_DISCOVERY_STATE_ANCS       1
    uint8_t                     discovery_state;

    uint8_t                     started;

    // Current value of the client configuration descriptor for characteristic 'Report'
    uint16_t                    ancs_s_handle;
    uint16_t                    ancs_e_handle;

    BD_ADDR                     remote_addr;   //address of currently connected client
    wiced_bt_ble_address_type_t addr_type;
} ancs_app_state_t;

#pragma pack()

// NVRAM save area
HOSTINFO ancs_hostinfo;

ancs_app_state_t ancs_app_state;

// Registration structure to be passed to the library

wiced_bt_ancs_reg_t ancs_client_reg;

#endif
